<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
    rel="stylesheet" type="text/css">
    <link href="assets/css/style.css" rel="stylesheet" type="text/css">
    <link href="assets/css/main.css" rel="stylesheet" type="text/css">-->
    <script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/assets/js/moment.min.js') }}"></script>
    <script src="{{ asset('/assets/js/daterangepicker.js') }}"></script>
    <script src="{{ asset('/assets/js/main.js') }}"></script>
    <script src="{{ asset('/assets/js/participante/script.js') }}"></script>
    
    
    
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/main.css" rel="stylesheet" type="text/css">
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-77818352-1', 'auto');
    ga('send', 'pageview');

  </script>
  </head><body>
    <div class="container-fluid">
       <div class="row">
      <img src="assets/images/winners.jpg" class="img-responsive full-width">
       </div>
    </div>
    
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center" id="title-participants">
          <br>
            <p class="cup-text color-red-imp">
              ¡Felicitaciones a los ganadores!
            </p>
            <p>Estos fueron los ganadores por semana en el concurso La foto millonaria</p>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">

          @if($competitions != null && count($competitions) > 0)
            @foreach($competitions->all() as $competition)
            <div class="col-md-2 text-center">
              @if($competition->btn_selected)
              <a class="btn-style btn-vote " href="/ganadores/?filter={{$competition->index}}" style="width: 180px!important;">
              {{$competition->name}}</a>
              @else
              <a class="btn-style btn-vote-negative text-center" href="/ganadores/?filter={{$competition->index}}" style="width: 180px!important;">
              {{$competition->name}}</a>
              @endif
              <br>
              &nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;Hasta el {{$competition->format_date}}
              <br>&nbsp;
            </div>
            @endforeach
          @endif
        </div>
      </div>
    </div> 
    <div class="section" id="participants-list">
      <div class="container">
        <div class="row" id="result_grid">

          @if($competitors != null && count($competitors) > 0)
            @foreach($competitors as $competitor)
          <div class="col-md-3 click-style"
           onclick="showModalProfile('{{ $competitor->id }}', 
           '{{$competitor->photo_id}}', 
           '{{ $competitor->first_name }} {{$competitor->last_name}}', 
           '{{$competitor -> master_city -> name }}', 
           '{{$competitor -> master_department -> name}}','{{ $competitor->votes }}','{{$competitor->description}}','{{$competitor->permalink}}')" >
            <a><img src="{{URL::asset('/img/'.$competitor->photo_id)}}" class="center-block img-responsive "></a>
            <h3 class="grid-text">
              {{ str_limit($competitor->first_name .' '. $competitor->last_name,$limit = 19) }}
            </h3>
            <p>
              {{ str_limit($competitor->description,$limit = 25) }}
              <br>
              
              <br>&nbsp;
            </p>
          </div>   
            @endforeach
          @endif

        </div>
      </div>
    </div>
    
    <script type="text/javascript">
      filter = '{!! $filter !!}';
      setDefaultButton();
    </script>

    @include('layout.modalwinnerprofile')
    

    <script type="text/javascript">
      
      $(document).ready(function(){
               
                 $('#avatar-modal').on('hidden.bs.modal', function () {
                    location.reload();
                });
                 $('#avatar-modal-btn-close').on('click', function () {
                      location.reload();
                });

                 

            });

    </script>

</body></html>