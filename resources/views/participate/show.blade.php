<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

        <meta property="og:title" content="¡Vota ya por {{ $competitor->first_name }}!" />
        <meta property="og:type" content="blog" />
        <meta property="og:url" content="http://www.florhuila.com/lafotomillonariaflorhuila/{{$competitor->permalink}}" />
        <meta property="og:site_name" content="Florhuila" />
        <meta property="fb:app_id" content="735818926486957" />
        <meta property="og:image" content="http://104.236.197.209/img/{{$competitor->photo_id}}" />
        <meta property="og:description" content="Ayuda a {{ $competitor->first_name }} a ganar $1'000.000 en el concurso La Foto Millonaria Florhuila ¡Ingresa aquí y vota!" />

        <link href="{{ asset('/assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/css/cropper.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/css/daterangepicker.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/css/main.css') }}" rel="stylesheet" type="text/css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media
        queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head><body>
        
        @include('layout.modalprofile')

        <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
        <script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/assets/js/cropper.min.js') }}"></script>
        <script src="{{ asset('/assets/js/moment.min.js') }}"></script>
        <script src="{{ asset('/assets/js/daterangepicker.js') }}"></script>
        <script src="{{ asset('/assets/js/main.js') }}"></script>
        <script src="{{ asset('/assets/js/participante/script.js') }}"></script>
    
        <script type="text/javascript">
            $(document).ready(function(){
                $('#avatar-modal').modal('show');

                showModalProfile('{{ $competitor->id }}', 
                   '{{$competitor->photo_id}}', 
                   '{{ $competitor->first_name }} {{$competitor->last_name}}', 
                   '{{$competitor -> master_city -> name }}', 
                   '{{$competitor -> master_department -> name}}','{{ $competitor->cant_votes }}','{{$competitor->description}}','{{$competitor->permalink}}');

                 $('#avatar-modal').on('hidden.bs.modal', function () {
                      window.location.href = "/#title-participants";
                });
                 $('#avatar-modal-btn-close').on('click', function () {
                      window.location.href = "/#title-participants";
                });
            });
        </script>
</body></html>