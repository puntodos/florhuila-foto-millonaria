<html><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/cropper.css" rel="stylesheet" type="text/css">
    <link href="assets/css/daterangepicker.css" rel="stylesheet" type="text/css">
    <link href="assets/css/main.css" rel="stylesheet" type="text/css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media
    queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head><body class="participants-background">
    <!--<div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class=" title-color title_register_form">FORMULARIO DE REGISTRO</h1>
          </div>
        </div>
      </div>
    </div>-->
    <div class="section">
      <div class="container">
        <div class="row">
          <div id="validate_errors"></div>
          @if(count($errors) > 0)
            <div class="alert alert-danger" role="alert">
              <ul>
              @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
              </ul>
            </div>
          @endif
          
          <!-- <div class="col-md-12">-->
          <br><br>
          {!! Form::open(['url' => '/participar', 'method' => 'post', 'id' => 'participar_form']) !!}
            <div class="jumbotron">
              <div class="container">
                <div class="row">
                  <div class="col-md-12">
                    <h1 class="title-color color-red-imp title_register_form">FORMULARIO DE REGISTRO</h1><br>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                      <div class="form-group">
                        {!! Form::text('first_name', null, array('class' => 'form-control','placeholder'=>'Nombre','maxlength'=>'30')) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::text('last_name', null, array('class' => 'form-control','placeholder'=>'Apellido','maxlength'=>'30')) !!}
                      </div>
                      <div class="form-group">
                        <!--<input class="form-control" id="idBornDate" placeholder="Fecha de nacimiento" type="text">-->
                         {!! Form::hidden('birth_date', '',['id'=>'birth_date']) !!}
                        
                          <!-- !! Form::text('birth_date', null, array('class' => 'form-control','readonly'=>'readonly', 'id' => 'birth_date','placeholder'=>'Seleccione su fecha de nacimiento','value'=>'')) !!}-->
                          <!--<span class="input-group-addon glyphicon glyphicon-calendar"></span>-->
                          
                         
                          <label class="col-sm-12" style="color:black;">Fecha de Nacimiento</label>
                          <div class="col-sm-3">
                            {!! Form::number('year', null, array('class' => 'form-control','placeholder'=>'Año','maxlength'=>'4','id'=>'year', 'onkeypress' => 'return isNumeric(event)','oninput'=>'maxLengthCheck(this)')) !!}
                          </div>
                          <div class="col-sm-2">
                            {!! Form::number('month', null, array('class' => 'form-control','placeholder'=>'Mes','maxlength'=>'2','id'=>'month','onkeypress' => 'return isNumeric(event)','oninput'=>'maxLengthCheck(this)')) !!}
                          </div>
                          <div class="col-sm-2">
                            {!! Form::number('day', null, array('class' => 'form-control','placeholder'=>'Dia','maxlength'=>'2','id'=>'day','onkeypress' => 'return isNumeric(event)','oninput'=>'maxLengthCheck(this)')) !!}
                            <div class="help">&nbsp;</div>
                          </div>
                       
                      </div>
                      <div class="form-group">
                        {!! Form::number('dni', null, ['class' => 'form-control','placeholder'=>'Cédula','maxlength'=>'20']) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::select('department', $departments, null, ['placeholder' => 'Seleccione un Departamento...','class' => 'form-control','id'=>'department'])  !!}
                      </div>
                      <div class="form-group">
                        <input type="hidden" value="{!! old('department') !!}" id="old_department" />
                        <input type="hidden" value="{!! old('city') !!}" id="old_city" />
                        {!! Form::select('city',$cities,null, ['placeholder' => 'Seleccione una Ciudad...','class' => 'form-control','id'=>'city'])  !!}
                        
                      </div>
                      <div class="form-group">
                        {!! Form::text('address', null, array('class' => 'form-control','placeholder'=>'Dirección:','maxlength'=>'100')) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::number('phone', null, array('class' => 'form-control','placeholder'=>'Teléfono','maxlength'=>'20')) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::email('email', null, array('class' => 'form-control','placeholder'=>'Correo','maxlength'=>'100')) !!}
                      </div>
                      <div class="form-group">
                        {!! Form::select('source', ['Punto de venta' => 'Punto de venta', 'Amigo /Referido' => 'Amigo /Referido','Publicidad'=>'Publicidad' ,'otro. '=>'otro. '], null, ['placeholder' => 'Fuente por donde se enteró del concurso...','class'=>'form-control']) !!}
                      </div>
                      <div class="form-group" style="color:black;">
                      Acepto términos y condiciones.
                      &nbsp;
                        <input type="checkbox" name="terms" id="terms">&nbsp;</div>
                      <div class="form-group">
                        <span class="color-red-imp">
                          <a href="/docs/terminosycondiciones.pdf" download>
                           Descargar PDF
                          </a>
                        </span>
                      </div>
                      <div class="form-group">&nbsp;</div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-md-offset-2 vcenter text-center">
                    <div class="form-group title-color">Subir foto</div>
                    <br><br>
                    <a class="up-arrow" id="btn-upload-photo">Examinar</a>
                     <div class="avatar-view text-center" id="avatar-view" title="Cambiar Imagen">
                        <img src="assets/images/watermark.png" alt="Avatar">
                        {!! Form::hidden('photo_id', '',['id'=>'photo_id']) !!}
                      </div>
                      <br><br><br><br>
                    <!--<img src="http://pingendo.github.io/pingendo-bootstrap/assets/placeholder.png" class="img-responsive ">-->
                  </div>
                  <div class="col-md-4 vcenter" style="color:black;">
                    <h2>Ten en cuenta lo siguiente para que tu foto sea válida:</h2>
                    <table class="table">
                      <tbody>
                        <tr>
                          <td class="td-center-vertical number-red">&nbsp;&nbsp;&nbsp;1.</td>
                          <td>
                            <p class="paragraph-options" style="color:black;">Mínimo dos personas en la foto. Entre más personas, ¡más posibilidades
                              de ganar!</p>
                          </td>
                        </tr>
                        <tr>
                          <td class="td-center-vertical number-red">&nbsp;&nbsp;&nbsp;2.</td>
                          <td>
                            <p class="paragraph-options" style="color:black;">Debe aparecer mínimo un producto o empaque Florhuila: ARROZ FLORHUILA PLATINO, ARROZ FLORHUILA INTEGRAL Y/O ARROZ FLORHUILA CON VITAMINAS.</p>
                          </td>
                        </tr>
                        <tr>
                          <td class="td-center-vertical number-red">&nbsp;&nbsp;&nbsp;3.</td>
                          <td>
                            <p class="paragraph-options" style="color:black;">Debe aparecer un plato con una receta a base de Arroz Florhuila</p>
                          </td>
                        </tr>
                        <tr>
                          <td class="td-center-vertical number-red">&nbsp;&nbsp;&nbsp;4.</td>
                          <td>
                            <p class="paragraph-options" style="color:black;">No se deben incluir o mencionar marcas o productos diferentes a los productos
                              de las extensiones de línea de la marca Florhuila</p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                  <label class="form-class" id="description_feed" style="color:black;">Describe tu foto  </label>
                    <div class="form-group">
                        {!! Form::textarea('description',null, ['class' => 'form-control','placeholder'=>'Ingresa aquí tu descripción','size'=>'1x3','maxlength'=>'140','id'=>'description']) !!}
                      </div>
                  </div>
                </div>
                <div class="row text-center">
                  <!--<a class="btn-style btn-red">Participar</a>-->
                  <!--{!! Form::button('Participar', array('class' => 'btn-style btn-red','id'=>'sendBtn')) !!}-->
                  <input type="submit" value="Participar" class="btn-style btn-red" />
                </div>
              </div>
            </div>
          <!--</div>-->
          {!! Form::close() !!}
        </div>
      </div>
    </div>
    
    <div class="container" id="crop-avatar">

    <!-- Cropping modal -->
    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <form class="avatar-form" action="crop.php" enctype="multipart/form-data" method="post">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="avatar-modal-label">
                <span class="form-group title-color">Subir foto</span>
              </h4>
            </div>
            <div class="modal-body">
              <div class="avatar-body">

                <!-- Upload image and data -->
                <div class="avatar-upload">
                  <input type="hidden" class="avatar-src" name="avatar_src">
                  <input type="hidden" class="avatar-data" name="avatar_data">
                  <a id="file-upload-p2" class="btn-style btn-red-upload-photo">Examinar</a>
                  <input type="file" class="avatar-input upload" id="avatarInput" name="avatar_file">
                </div>
                <!--<div id="id-loading" class="text-center" style="">-
                Cargando....<img src="/assets/images/loading.gif" />
                </div>-->
                <div class="text-center" style="color:black!important;">Peso máximo de la foto 5MB</div>
                <!-- Crop and preview -->
                <div class="row">
                  <div class="col-md-9">
                    <div class="avatar-wrapper"></div>
                  </div>
                  <div class="col-md-3">
                    <div class="avatar-preview preview-lg"></div>
                    <!--<div class="avatar-preview preview-md"></div>
                    <div class="avatar-preview preview-sm"></div>-->
                    <img src="/assets/images/watermark.png" class="watermark" />
                  </div>
                </div>

                <div class="row avatar-btns">
                  <div class="col-md-9">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-primary-red" data-method="rotate" data-option="-90" title="Girar a la Izquierda">Girar a la Izquierda</button>
                      <button type="button" class="btn btn-primary btn-primary-red" data-method="rotate" data-option="90">Girar a la Derecha</button>
                    </div>

                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-primary-red" data-method="rotate" data-option="0.1" data-invader="zoom" title="Girar a la Izquierda"><span class="fa fa-search-plus"></span>&nbsp;Aumentar</button>
                      <button type="button" class="btn btn-primary btn-primary-red" data-method="rotate" data-option="-0.1" data-invader="zoom"><span class="fa fa-search-minus"></span>&nbsp;Disminuir</button>
                    </div>

                  </div>
                  <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-primary-red btn-block avatar-save">Terminar Edición</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> -->
          </form>
        </div>
      </div>
    </div><!-- /.modal -->

    <!-- Loading state -->
    
  </div>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/cropper.min.js"></script>
    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/daterangepicker.js"></script>
    <script src="assets/js/bootstrap-waitingfor.min.js"></script>
    
    
    <script src="assets/js/main.js"></script>

    <script>
    var year = "", month = "", day = "";

    function maxLengthCheck(object) {
      if (object.value.length > object.maxLength){
        object.value = object.value.slice(0, object.maxLength)
      }
        if(object.id == "year"){
          year = object.value;
        }
        if(object.id == "month"){
          month = object.value;
        }
        if(object.id == "day"){
          day = object.value;
        }
        $("#birth_date").val(year+"-"+month+"-"+day); 
      
    }
      
    function isNumeric (evt) {
      var theEvent = evt || window.event;
      var key = theEvent.keyCode || theEvent.which;
      key = String.fromCharCode (key);//|\.
      var regex = /[0-9]/;
      if ( !regex.test(key) ) {
        theEvent.returnValue = false;
        if(theEvent.preventDefault) theEvent.preventDefault();
      }
    }

    </script>
    
 

</body></html>