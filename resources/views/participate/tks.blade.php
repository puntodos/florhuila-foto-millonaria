<html><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="{{ asset('/assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/css/cropper.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/css/daterangepicker.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/assets/css/main.css') }}" rel="stylesheet" type="text/css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media
        queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
    </head><body>
        
                <div class="modal-background modal fade participant-back" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-lg ">
                <div class="modal-content-v2 modal-content modalPicture">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="close-V2 close" data-dismiss="modal">×</button>
                        </div>
                    </div>
                    
                    <div class="modal-body">
                        <div class="row" id="body_tks_profile_modal">
                        <br>
                            <div class="col-md-12 text-center tks-text-white" id="modal_result_vote">
                              ¡Gracias por participar en La Foto Millonaria Florhuila! Muy pronto tu foto estará disponible y te lo estaremos notificando en tu correo electrónico para que votes y la compartas con tus familiares y amigos.

                            </div>
                            <div class="col-md-12 text-center tks-text-white">
                            <br>
                                      <a id="avatar-modal-btn-close" class="btn-style btn-white text-center" style="width: 300px!important;">Continuar</a><br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
        <script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
        <script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
        <!--<script src="{{ asset('/assets/js/cropper.min.js') }}"></script>-->
        <!--<script src="{{ asset('/assets/js/moment.min.js') }}"></script>-->
        <!--<script src="{{ asset('/assets/js/daterangepicker.js') }}"></script>-->
        <!--<script src="{{ asset('/assets/js/main.js') }}"></script>-->
        <!--<script src="{{ asset('/assets/js/participante/script.js') }}"></script>-->
    
        <script type="text/javascript">
            $(document).ready(function(){
                $('#avatar-modal').modal('show');
                $("#modal_result_vote").show();

                 $('#avatar-modal').on('hidden.bs.modal', function () {
                      window.location.href = "/#title-participants";
                });
                 $('#avatar-modal-btn-close').on('click', function () {
                      window.location.href = "/#title-participants";
                });
            });
        </script>
</body></html>