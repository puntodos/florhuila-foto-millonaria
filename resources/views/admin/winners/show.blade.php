@extends('layout.app')

@section('content')

    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <span>FILTRO POR &nbsp;&nbsp;&nbsp;</span>
            <div class="btn-group btn-group-lg">
              
            </div>
          </div>
        </div>
      </div>
    </div>
    @if(count($competitors) > 0)
            @foreach($competitors->all() as $competitor)
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <img src="{{URL::asset('/img/'.$competitor->photo_id)}}"
            class="img-responsive">
          </div>
          <div class="col-md-6">
            <p>
              <span class="bold">Nombre:</span> {{ $competitor->first_name }}<br>
              <span class="bold">Apellido:</span> {{ $competitor->last_name }}<br>
              <span class="bold">Cédula:</span> {{ $competitor->dni }}<br>
              <span class="bold">Teléfono:</span> {{ $competitor->phone }}<br>
              <span class="bold">Fecha de Nacimiento:</span> {{ $competitor->birth_date }}<br>
              <span class="bold">Departamento:</span> {{ $competitor->master_department->name }}<br>
              <span class="bold">Ciudad:</span> {{ $competitor->master_city->name }}<br>
              <span class="bold">Email:</span> {{ $competitor->email }}<br>
              <span class="bold">Fuente:</span> {{ $competitor->source }}<br>
              <span class="bold">Descripción:</span> {{ $competitor->description }}<br>
            </p>
          </div>
          <div class="col-md-2">
            <p>
              
               <input type="checkbox" name="status" id="status_{{ $competitor->competitor_id }}" onclick="winnerChangeStatus({{$competitor -> competition_id}},{{$competitor -> competitor_id}})" {{ ($competitor -> text_status == "Ganador") ? 'checked' : '' }} >&nbsp;
               <span id="text_status_{{ $competitor->competitor_id }}">{{ $competitor -> text_status }}</span>
            </p>
          </div>
        </div>
      </div>
    </div>


        @endforeach
    @endif
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center" id="">
            
          </div>
        </div>
      </div>
    </div>

@endsection