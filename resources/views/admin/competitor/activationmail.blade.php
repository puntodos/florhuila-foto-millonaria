<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title></title>
	<style type="text/css">
  #outlook a { padding: 0; }
  .ReadMsgBody { width: 100%; }
  .ExternalClass { width: 100%; }
  .ExternalClass * { line-height:100%; }
  body { margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
  table, td { border-collapse:collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
  img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
  p { display: block; margin: 13px 0; }
</style>
	<!--[if !mso]>
	<!-->
	<style type="text/css">
  @media only screen and (max-width:480px) {
    @-ms-viewport { width:320px; }
    @viewport { width:320px; }
  }
</style>
	<!--<![endif]-->
	<style type="text/css">
  @media only screen and (min-width:480px) {
    .mj-column-per-100, * [aria-labelledby="mj-column-per-100"] { width:100%!important; }
  }
</style>
</head>
<body>
	<div>
		<!--[if mso]>
		<table border="0" cellpadding="0" cellspacing="0" width="600" align="center" style="width:600px;">
			<tr>
				<td>
					<![endif]-->
					<div style="margin:0 auto;max-width:600px;">
						<table cellpadding="0" cellspacing="0" style="font-size:1px;width:100%;" align="center" border="0">
							<tbody>
								<tr>
<td style="text-align:center;vertical-align:top;font-size:1px;padding:20px 0px;">
	<!--[if mso]>
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td style="vertical-align:top;width:600px;">
				<![endif]-->
				<div aria-labelledby="mj-column-per-100" class="mj-column-per-100" style="vertical-align:top;display:inline-block;font-size:13px;text-align:left;width:100%;">
					<table cellpadding="0" cellspacing="0" width="100%" border="0">
						<tbody>
							<tr>
								<td style="word-break:break-word;font-size:1px;padding:10px 25px;text-align:center;">
									<table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px;" align="center" border="0">
										<tbody>
											<tr>
												<td style="width:550px;">
													<img alt="" height="auto" src="http://www.florhuila.com/wp-content/uploads/mail.header.png" style="border:none;display:block;outline:none;text-decoration:none;width:100%;height:auto;" width="550"></td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:1px;padding:10px 25px;text-align:center;">
									<div style="cursor:auto;color:black;font-family:helvetica;font-size:20px;font-weight:bold;line-height:22px;">
										Hola {{$name }} ¡Traemos buenas noticias para ti!
									</div>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:1px;padding:10px 25px;text-align:left;">
									<div style="cursor:auto;color:black;font-family:helvetica;font-size:15px;line-height:22px;">
										Tu Foto Millonaria ya está publicada en nuestro sitio web. No pierdas tiempo y compártela ya mismo con tu familia y amigos para que voten por ti, y puedas ser el posible ganador de $1.000.000 (aplican condiciones y restricciones. Más información en www.florhuila.com).

Puedes ver tu foto ingresando a:
										<a href="http://www.florhuila.com/lafotomillonariaflorhuila/{{ $permalink }}">Tu Foto Aquí</a>
										
									</div>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:1px;padding:10px 25px;text-align:left;">
									<div style="cursor:auto;color:black;font-family:helvetica;font-size:15px;line-height:22px;">Cordialmente.</div>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:1px;padding:10px 25px;text-align:left;">
									<div style="cursor:auto;color:black;font-family:helvetica;font-size:16px;font-weight:bold;line-height:22px;">Equipo Foto Millonaria Florhuila</div>
								</td>
							</tr>
							<tr>
								<td style="word-break:break-word;font-size:1px;padding:10px 25px;text-align:left;">
									<div style="cursor:auto;color:black;font-family:helvetica;font-size:11px;line-height:22px;">
										Nota: Este correo es informativo. Favor no responder a esta dirección de correo, ya que no se encuentra habilitada para recibir mensajes.
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--[if mso]>
			</td>
		</tr>
	</table>
	<![endif]-->
</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!--[if mso]>
				</td>
			</tr>
		</table>
		<![endif]-->
	</div>
</body>
</html>