@extends('layout.app')

@section('content')

<div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <span>FILTRO POR &nbsp;&nbsp;&nbsp;</span>
            <div class="btn-group btn-group-lg">
              <select id="filter" name="filter" onchange="changeFilter()"class="form-control">
              
                <option value="P" {{ $filter_selected_P }}>Pendientes</option>
                <option value="A" {{ $filter_selected_A }}>Activos</option>
                <option value="I" {{ $filter_selected_I }}>Inactivos</option>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
    @if(count($competitors) > 0)
            @foreach($competitors->all() as $competitor)
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <hr>
          </div>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <img src="{{URL::asset('/img/'.$competitor->photo_id)}}"
            class="img-responsive">
          </div>
          <div class="col-md-6">
            <p>
              <span class="bold">Nombre:</span> {{ $competitor->first_name }}<br>
              <span class="bold">Apellido:</span> {{ $competitor->last_name }}<br>
              <span class="bold">Cédula:</span> {{ $competitor->dni }}<br>
              <span class="bold">Teléfono:</span> {{ $competitor->phone }}<br>
              <span class="bold">Fecha de Nacimiento:</span> {{ $competitor->birth_date }}<br>
              <span class="bold">Departamento:</span> {{ $competitor->master_department->name }}<br>
              <span class="bold">Ciudad:</span> {{ $competitor->master_city->name }}<br>
              <span class="bold">Email:</span> {{ $competitor->email }}<br>
              <span class="bold">Fuente:</span> {{ $competitor->source }}<br>
              <span class="bold">Descripción:</span> {{ $competitor->description }}<br>
            </p>
          </div>
          <div class="col-md-2">
            <p>
            <span class="bold">Estado: </span>
            <span id="text_status_{{ $competitor->id }}">{{ $competitor -> status_text }}</span><br><br>
            <span class="bold">Cambiar estado: </span><br>
              <input type="radio" name="status_{{ $competitor->id }}" id="status_{{ $competitor->id }}" value="A" onchange="changeStatus({{$competitor -> id}})" {{ ($filter_selected_A == 'selected') ? 'checked' : '' }} />
              &nbsp;Activar<br>
              <input type="radio" name="status_{{ $competitor->id }}" id="status_{{ $competitor->id }}" value="I" onchange="changeStatus({{$competitor -> id}})" {{ ($filter_selected_I == 'selected') ? 'checked' : '' }} />
              &nbsp;Desactivar
              <br>
            </p>
          </div>
        </div>
      </div>
    </div>


        @endforeach
    @endif
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center" id="pagination">
            {!! $competitors->render() !!}
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      filter = '{!! $filter !!}';
    </script>

@endsection