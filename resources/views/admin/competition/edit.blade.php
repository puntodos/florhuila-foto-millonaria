@extends('layout.app')

@section('content')


<div class="section" id="welcome-text">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
              {!! Form::open(['route' => 'admin.competition.update', 'method' => 'post', 'id' => 'participar_form']) !!}
              {{ Form::hidden('id', $competition->id) }}
              {{ Form::hidden('action','edit', ['id'=>'action']) }}
              <table class="table table-bordered"> 
                <thead> 
                  <tr> 
                    <th>Campo</th> 
                    <th>Valor</th> 
                    
                  </tr> 
                </thead> 
                <tbody> 
                  <tr> 
                    <th scope="row">Nombre</th> 
                    <td>
                    {!! Form::text('name', $competition->name, array('class' => 'form-control','placeholder'=>'Nombre','maxlength'=>'30')) !!}
                    </td> 
                  </tr> 
                  <tr> 
                    <th scope="row">Desde</th> 
                    <td>
                      <div class="input-group">
                      {!! Form::text('date_init', null, array('class' => 'form-control','readonly'=>'readonly', 'id' => 'date_init','placeholder'=>'Seleccione la fecha de inicio','value'=>$competition->date_init)) !!}
                      <span class="input-group-addon glyphicon glyphicon-calendar"></span>
                      </div>
                    </td> 
                  </tr> 
                  <tr> 
                    <th scope="row">Hasta</th> 
                    <td>
                      <div class="input-group">
                      {!! Form::text('date_end', null, array('class' => 'form-control','readonly'=>'readonly', 'id' => 'date_end','placeholder'=>'Seleccione la fecha de fin','value'=>$competition->date_end)) !!}
                      <span class="input-group-addon glyphicon glyphicon-calendar"></span>
                      </div>
                    </td> 
                  </tr> 
                  <tr> 
                    <td colspan="2" class="text-center">
                      {!! Form::submit('Actualizar', array('class' => 'btn btn-success btn-lg','id'=>'sendBtn')) !!}

                    </td> 
                  </tr> 
                </tbody> 
              </table>
              {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>






@endsection