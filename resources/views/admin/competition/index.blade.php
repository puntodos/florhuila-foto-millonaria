@extends('layout.app')

@section('content')


<div class="section" id="welcome-text">
    <div class="container">
    	<div class="row text-center">
          	<div class="col-md-6 col-md-offset-3">
          		<a class="btn btn-primary btn-lg" href="{{ route('admin.competition.create') }}" role="button">Crear Nuevo</a>
          	</div>
        </div> 	
        <div class="row text-center">
          	<div class="col-md-6 col-md-offset-3">
          		&nbsp;
          	</div>
        </div> 	

        <div class="row">
          	<div class="col-md-6 col-md-offset-3">
          		@if(count($competitions_all) > 0)
          		<table class="table table-bordered"> 
          			<thead> 
          				<tr> 
          					<th>#</th> 
          					<th>Nombre</th> 
          					<th>Desde</th> 
          					<th>Hasta</th> 
          					<th></th> 
          					
          				</tr> 
          			</thead> 
          			<tbody> 
          				@foreach($competitions_all->all() as $competition)
          				<tr> 
          					<th scope="row">{{$competition->id}}</th> 
      						<td>{{$competition->name}}</td> 
      						<td>{{$competition->date_init}}</td> 
      						<td>{{$competition->date_end}}</td> 
      						<td><a class="label label-warning" href=" {{route('admin.competition.edit',['id'=>$competition->id]) }}">editar</a></td>
          				</tr> 
          				@endforeach
          			</tbody> 
          		</table>
          		@endif
          	</div>
        </div>
    </div>
</div>






@endsection