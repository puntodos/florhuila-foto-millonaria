<div class="modal-background modal fade" id="info-modal" aria-hidden="true" aria-labelledby="faq-modal-label" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg ">
    <div class="modal-content-v2 modal-content">
      <div class="row">
        <div class="col-md-12">
          <button type="button" class="close-V3 close" data-dismiss="modal">×</button>
        </div>
      </div>

      <div class="modal-body">

        <div class="jumbotron jumbotron-rounder">
          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-center text-red">
              <div>SI NO HAS GANADO PUEDES SEGUIR PARTICIPANDO POR PREMIOS.</div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-black">
              <br>
              Siempre vas a tener la posibilidad de seguir participando y poder ganar. Cada semana el contador de votos se reiniciará en Cero (0), de tal manera que si no alcanzaste en una semana a tener la mayor cantidad de votos, la siguiente lo puedes volver a intentar. Las siguientes son las fechas de selección.
            </div>
          </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-black">
              <table class="table">
                <tbody>
                  <tr>
                    <td class="td-center-vertical text-black">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    Primera Selección
                    </td>
                    <td class="td-center-vertical text-black">
                    16 de Mayo 2016  – 02 de Junio de 2016 - 8:00am
                    </td>
                  </tr>
                  <tr>
                    <td class="td-center-vertical text-black">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    Segunda Selección
                    </td>
                    <td class="td-center-vertical text-black">
                    03 de Junio 2016  – 09 de Junio de 2016 -  8:00am
                    </td>
                  </tr>
                  <tr>
                    <td class="td-center-vertical text-black">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    Tercera Selección
                    </td>
                    <td class="td-center-vertical text-black">
                    10 de Junio 2016  – 16 de Junio de 2016  - 8:00am
                    </td>
                  </tr>
                  <tr>
                    <td class="td-center-vertical text-black">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    Cuarta Selección
                    </td>
                    <td class="td-center-vertical text-black">
                    17 de Junio 2016  – 23 de Junio de 2016 - 8:00am
                    </td>
                  </tr>
                  <tr>
                    <td class="td-center-vertical text-black">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    Quinta Selección
                    </td>
                    <td class="td-center-vertical text-black">
                    24 de Junio 2016  – 30 de Junio de 2016 - 8:00am
                    </td>
                  </tr>
                  <tr>
                    <td class="td-center-vertical text-black">
                    <i class="fa fa-check" aria-hidden="true"></i>
                    Sexta Selección
                    </td>
                    <td class="td-center-vertical text-black">
                    01 de Julio 2016 – 07 de Julio de 2016 - 8:00am
                    </td>
                  </tr>

                </tbody>
              </table>
            </div> 
          </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-black">
              Recuerda que son 30 Pre-seleccionados por votación cada semana; y de estos 30 concursantes escogemos los 10 más creativos y auténticos según los siguientes parámetros:<br>
            </div>
          </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-black">
              <table class="table">
                <tbody>
                  <tr>
                    <td class=" text-black"><br>

                      <i class="fa fa-circle" class="font-size:0.5em;" aria-hidden="true"></i>
                     Creatividad y originalidad de la foto en general (30%)
                    </td>
                  </tr>
                  <tr>
                    <td class=" text-black">
                    <i class="fa fa-circle" class="font-size:0.5em;" aria-hidden="true"></i>
                    Mayor Cantidad de miembros de la familia participantes en la fotografía (20%)
                    </td>
                  </tr>
                  <tr>
                    <td class=" text-black">
                    <i class="fa fa-circle" class="font-size:0.5em;" aria-hidden="true"></i>
                    Apetitosidad del Plato de arroz y complejidad de la receta (30%)
                    </td>
                  </tr>
                  <tr>
                    <td class=" text-black">
                    <i class="fa fa-circle" class="font-size:0.5em;" aria-hidden="true"></i>
                    Creatividad en la reseña del mensaje y descripción de la foto (20%)
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-black">
              <br>
              Así que no te desanimes, ¡Tienes muchas oportunidades para GANAR!
            </div>
          </div>
          <div class="row text-right">
            <a class="btn-style btn-vote text-center" onclick="closeInfoModal()">Volver</a>
            &nbsp;&nbsp;&nbsp;&nbsp;
          </div>
        </div><!--jumbutron-->

      </div><!--modal body-->
    </div><!--modalcontent-->
  </div><!--modal dialog-->
</div>