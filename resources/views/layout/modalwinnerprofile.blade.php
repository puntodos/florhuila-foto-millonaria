        <div class="modal-background modal fade participant-back" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-lg ">
                <div class="modal-content-v2 modal-content modalPicture">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="close-V2 close" data-dismiss="modal">×</button>
                        </div>
                    </div>
                    
                    <div class="modal-body">
                        <div class="avatar-body" id="body_profile_modal">
                                <div class="row" >
                                    <div class="col-md-12 text-center">
                                        
                                              <img id="modal_image" src="" class="center-block img-responsive modalImage">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 title-white-participant title_respect_image">
                                        <span id="modal_name"></span>
                                        <input type="hidden" id="participant_id" 
                                        value=""/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 text-white-participant title_respect_image">
                                        <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>&nbsp;
                                        <span id="modal_city"></span>&nbsp;
                                        <span id="modal_department"></span>
                                    </div>
                                </div>
                                <div style="display: none;" class="row">
                                    <div class="col-md-2 title-white-participant title_respect_image">
                                            <i class="fa fa-heart-white fa-heart" aria-hidden="true"></i>
                                            Votos
                                            <span  id="participant_votes_cant">
                                                
                                            </span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 description-box" id="participant_description">
                                    
                                    </div>
                                </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>