        <div class="modal-background modal fade participant-back" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-lg ">
                <div class="modal-content-v2 modal-content modalPicture">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="close-V2 close" data-dismiss="modal">×</button>
                        </div>
                    </div>
                    
                    <div class="modal-body">
                        <div class="avatar-body" id="body_profile_modal">
                                <div class="row" >
                                    <div class="col-md-12 text-center">
                                        
                                              <img id="modal_image" src="" class="center-block img-responsive modalImage">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 title-white-participant title_respect_image">
                                        <span id="modal_name"></span>
                                        <input type="hidden" id="participant_id" 
                                        value=""/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 text-white-participant title_respect_image">
                                        <i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>&nbsp;
                                        <span id="modal_city"></span>&nbsp;
                                        <span id="modal_department"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2 title-white-participant title_respect_image">
                                        <a class="btn-style-participant btn-white-participant" id="participant_vote">
                                            <i class="fa fa-heart" aria-hidden="true"></i>
                                            <span id="participant_votes_cant">
                                                
                                            </span>
                                         Votos</a>
                                    </div>
                                    <div class="col-md-2 col-md-offset-3 title-white-participant title_respect_image_fb">
                                        <a id="facebook_share" class="btn-style-participant btn-white-participant" href="" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                        Compartir</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 description-box" id="participant_description">
                                    
                                    </div>
                                </div>
                        </div>
                        <div class="row" id="body_tks_profile_modal">
                        <br><br><br>
                            <div class="col-md-12 text-center tks-text-white" id="modal_result_vote"></div>
                            <div class="col-md-12 text-center tks-text-white">
                            <br>
                            <a id="facebook_share_2" class="btn-style-participant btn-white-participant" href="" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                        Compartir</a>
                                        <br><br>
                                      <a id="avatar-modal-btn-close" class="btn-style btn-white text-center" style="width: 300px!important;">Continuar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>