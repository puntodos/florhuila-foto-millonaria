<div class="modal-background modal fade" id="faq-modal" aria-hidden="true" aria-labelledby="faq-modal-label" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content-v2 modal-content">
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="close-V3 close" data-dismiss="modal">×</button>
                </div>
            </div>
            
            <div class="modal-body">
        		
				      <div class="jumbotron jumbotron-rounder">
				        <div class="row">
				          <div class="col-md-8 col-md-offset-2">
				            <h1 class="text-center color-red-imp title-color title_faq_underline">Preguntas Frecuentes</h1>
				          </div>
				        </div>

				<div class="row">
                  <div class="col-md-8 col-md-offset-2 text-center">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td class="td-center-vertical title-participants-inst">1.</td>
                          <td>
                            <p class="paragraph-options">
                              <div class="title-participants-inst">
                                Tómate una foto con tu familia.
                              </div>
                              <div class="body-participants-inst">
                                con un plato de arroz Florhuila y la presencia de cualquier empaque de la marca.
                              </div>
                            </p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-8 col-md-offset-2 text-center">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td class="td-center-vertical title-participants-inst">2.</td>
                          <td>
                            <p class="paragraph-options">
                              <div class="title-participants-inst">
                                Tómate una foto con tu familia.
                              </div>
                              <div class="body-participants-inst">
                                con un plato de arroz Florhuila y la presencia de cualquier empaque de la marca.
                              </div>
                            </p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-8 col-md-offset-2 text-center">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td class="td-center-vertical title-participants-inst">3.</td>
                          <td>
                            <p class="paragraph-options">
                              <div class="title-participants-inst">
                                Tómate una foto con tu familia.
                              </div>
                              <div class="body-participants-inst">
                                con un plato de arroz Florhuila y la presencia de cualquier empaque de la marca.
                              </div>
                            </p>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
              </div>
              <div class="row text-right">
              		<a class="btn-style btn-vote text-center" onclick="closeFaqModal()">Volver</a>&nbsp;&nbsp;&nbsp;&nbsp;
              </div>
				      </div>
				    
            </div>
        </div>
    </div>
</div>    