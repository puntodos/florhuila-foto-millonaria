<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!--<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
    rel="stylesheet" type="text/css">
<link href="assets/css/style.css" rel="stylesheet" type="text/css">
<link href="assets/css/main.css" rel="stylesheet" type="text/css">
-->
<script src="{{ asset('/assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/js/cropper.min.js') }}"></script>
<script src="{{ asset('/assets/js/moment.min.js') }}"></script>
<script src="{{ asset('/assets/js/daterangepicker.js') }}"></script>
<script src="{{ asset('/assets/js/main.js') }}"></script>
<script src="{{ asset('/assets/js/participante/script.js') }}"></script>
<script src="{{ asset('/assets/js/jquery.countdown.js') }}"></script>

<link href="{{ asset('/assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/assets/css/main.css') }}" rel="stylesheet" type="text/css"></head>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-77818352-1', 'auto');
  ga('send', 'pageview');

</script>
<body>
<div class="container-fluid background-red">
       <div class="row">
      <img src="assets/images/home.png" class="img-responsive full-width">
       </div>
    </div>
<div class="section background-red" id="welcome-text">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="text-center">
          <br>
          <br>
          <br>
          <br>
          @if($competitions_end > 0)
          <a href="/ganadores">
            <img class="btn-image" src="/assets/images/ganadores.png" />
          </a>
          @endif
          <a style="width: 400px;" href="#how-to-participe">
            <img class="btn-image" src="/assets/images/how-to-participe.png" />
          </a>
          <a href="#title-participants">
            <img class="btn-image" src="/assets/images/votar.png" />
          </a>
          
          <br>
          <br>
          <br>
          <br></p>
      </div>
    </div>
  </div>
</div>
<div class="section background-red" id="how-to-participe">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <h1 class="text-center color-white title-color title_how_to_part_underline">
          <br>¿CÓMO PARTICIPAR?</h1>
      </div>
    </div>
    <!--<div class="row">
    <div class="col-md-6 col-md-offset-3">
      <img src="assets/images/title-line.png"></div>
  </div>
  -->
</div>
</div>
<div class="section background-red" id="ways-to-participe">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="section">
        <div class="container">
          <br>
          <br>
          <div class="row">
            <div class="col-md-4 col-md-offset-1 text-center">
              <table class="table">
                <tbody>
                  <tr>
                    <td class="td-center-vertical color-yellow title-participants-inst">
                      <img src="/assets/images/uno.png" class="number-img"/>
                    </td>
                    <td>
                      <p class="paragraph-options">
                        <div class="title-participants-inst color-yellow">Tómate una foto con tu familia.</div>
                        <div class="body-participants-inst color-white">
                           Con un plato de Arroz Florhuila y presencia de cualquier empaque de la marca.
                        </div>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-md-4 col-md-offset-1">
              <table class="table">
                <tbody>
                  <tr>
                    <td class="td-center-vertical color-yellow title-participants-inst">
                      <img src="/assets/images/dos.png" class="number-img"/>
                    </td>
                    <td>
                      <p class="paragraph-options">
                        <div class="title-participants-inst color-yellow">
                          Regístrate y Sube tu Foto 
                        </div>
                        <div class="body-participants-inst color-white">llena el formulario que se despliega en el botón de Participa Ya.</div>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-md-offset-1 text-center">
              <table class="table">
                <tbody>
                  <tr>
                    <td class="td-center-vertical color-yellow title-participants-inst">
                      <img src="/assets/images/tres.png" class="number-img"/>
                    </td>
                    <td>
                      <p class="paragraph-options">
                        <div class="title-participants-inst color-yellow">
                           Invita a tu familia y amigos para que voten por tu foto.
                        </div>
                        <div class="body-participants-inst color-white">Entre más votos tengas, más posibilidades de Ganar. Comparte en tus redes sociales.</div>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-md-4 col-md-offset-1">
              <table class="table">
                <tbody>
                  <tr>
                    <td class="td-center-vertical color-yellow title-participants-inst">
                      <img src="/assets/images/cuatro.png" class="number-img"/>
                    </td>
                    <td>
                      <p class="paragraph-options">
                        <div class="title-participants-inst color-yellow"> Pre-Seleccionaremos los 30 participantes con mayor cantidad de Votos cada semana*</div>
                        <div class="body-participants-inst color-white">Con la verificación de un Delegado de la Secretaria de Gobierno se hará este proceso</div>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-md-offset-1 text-center">
              <table class="table">
                <tbody>
                  <tr>
                    <td class="td-center-vertical color-yellow title-participants-inst">
                      <img src="/assets/images/cinco.png" class="number-img"/>
                    </td>
                    <td>
                      <p class="paragraph-options">
                        <div class="title-participants-inst color-yellow"> Se escogerán las 10 fotos más creativas y originales como Ganadores.</div>
                        <div class="body-participants-inst color-white">
                          Se tendrá en cuenta creatividad y originalidad 30%, Cantidad de personas en la fotografía 20%, apetitosidad de la receta 30% y creatividad en la descripción de la Foto 20%.
                        </div>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-md-4 col-md-offset-1 text-center">
              <table class="table">
                <tbody>
                  <tr>
                    <td class="td-center-vertical color-yellow title-participants-inst">
                      <img src="/assets/images/seis.png" class="number-img"/>
                    </td>
                    <td>
                      <p class="paragraph-options"><br><br><br>
                        <div class="title-participants-inst color-yellow">
                           PREMIO: Una Tarjeta débito Gift Card Diners de consumo cargada con $1.000.000.
                        </div>
                        <div class="body-participants-inst color-white">
                          
                        </div>
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!--seccion-->

<div class="section background-red" id="call-to-action">
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <a  href="/participar">
        <img class="btn-image" src="/assets/images/participa-ya.png" />
      </a>
      <br>&nbsp;</div>
  </div>
</div>
</div>
<div class="section" >
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="row"></div>
      <div class="row">
        <div class="col-md-12">
          <img src="assets/images/calendar-clock.png" class="center-block img-responsive"></div>
      </div>
      <div class="row">
        @foreach($competitions_names as $competition_name)
          @if($competition_name->current)
        <div class=" col-md-2 text-center ">
        <a class="btn-short btn-style btn-red nohover btn-red-hoverforce">{{$competition_name->name}}</a>
        </div>
          @else
        <div class=" col-md-2 text-center ">
        <a class="btn-short btn-style btn-gray nohover btn-gray-hoverforce">{{$competition_name->name}}</a>
        </div>
          @endif
        @endforeach

      </div>
      <div class="row text-center">
        <span class="text-red hour-red">
          &nbsp;
          <span id="getting-started"></span>
          &nbsp;
        </span>
        <script type="text/javascript">
                $('#getting-started').countdown('{{ $date_end }}', function(event) {
                  
                  $(this).html(event.strftime('%D días %H horas %M minutos %S segundos'));
                });
              </script>
      </div>
      <div class="row text-center text-red">
        Falta poco tiempo para saber cuáles son los ganadores de esta semana.
        <br>
        <br>
        <a class="btn-style btn-vote text-center" onclick="showInfoModal()" style="width: 300px!important;">Si no has ganado, sigue participando.</a>
        <br></div>
    </div>
  </div>
</div>
</div>
<div class="section">
<div class="container">
  <div class="row">
    <div class="col-md-12" id="title-participants">
      <h1 class=" title-color title_participants_underline">PARTICIPANTES</h1>
    </div>
  </div>
</div>
</div>
<div class="section">
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">

      <a class="btn-style btn-gray" id="themost">Los más votados</a>
      <a class="btn-style btn-gray" id="thelast">Los más recientes</a>
      <br>&nbsp;
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 text-center">
      <form class="search">
         <input type="text" id="search_query" placeholder="Buscar..." maxlength="50" />
         <a id="search" class="btn-style btn-vote text-center">Buscar</a>
       </form>
       <br><br>
    </div>
  </div>
</div>
</div>
<div class="section" id="participants-list">
<div class="container">
  <div class="row" id="result_grid">
    @if($competitors != null && count($competitors) > 0)
            @foreach($competitors->all() as $competitor)
    <div class="col-md-3 click-style"
           onclick="showModalProfile('{{ $competitor->
      competitor_id }}', 
           '{{$competitor->photo_id}}', 
           '{{ $competitor->first_name }} {{$competitor->last_name}}', 
           '{{$competitor -> master_city -> name }}', 
           '{{$competitor -> master_department -> name}}','{{ $competitor->votes }}','{{$competitor->description}}','{{$competitor->permalink}}')" >
      <a>
        <img src="{{URL::asset('/img/'.$competitor->photo_id)}}" class="center-block img-responsive "></a>
      <h3 class="grid-text">
        {{ str_limit($competitor->first_name .' '. $competitor->last_name,$limit = 19) }}
      </h3>
      <p>
        {{ str_limit($competitor->description,$limit = 25) }}
        <br>
        <a class="btn-style btn-vote text-center">Votar</a> <i class="fa fa-heart" aria-hidden="true">&nbsp;
          <span class="grid-count">{{ $competitor->votes }}</span></i> 
        <br>&nbsp;</p>
    </div>
    @endforeach
          @else
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
          @if($filter == "search")
            <p class="cup-text text-center">No se encontraron resultados</p>
          @else
            <p class="cup-text text-center">Votaciones cerradas temporalmente</p>
          @endif
          </div>
        </div>
      </div>
    </div>
    @endif
  </div>
</div>
</div>
<div class="section">
<div class="container">
  <div class="row">
    <div class="col-md-12 text-center" id="pagination">
    @if($competitors != null && count($competitors) > 0)
      {!! $competitors->render() !!}
    @endif
    </div>
  </div>
</div>
</div>
@if($competitions_end > 0)
<div class="section">
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <a href="/ganadores">
        <img src="assets/images/cup.png" class="center-block img-responsive">
      </a>
    </div>
  </div>
</div>
</div>
<div class="section">
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <a class="simple-link" href="/ganadores">
        <p class="cup-text text-center">Entra aquí y conoce los ganadores de las mejores recetas.</p>
      </a>
    </div>
  </div>
</div>
</div>
@endif
<script type="text/javascript">
      filter = '{!! $filter !!}';
      setDefaultButton();
    </script>

    @include('layout.modalprofile')

    @include('layout.info')
    
    

<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2 text-center">
      <br>
      <a class="btn-style btn-vote text-center" style="width: 200px!important;" href="/docs/preguntasfrecuentes.pdf" download>
                           Preguntas Frecuentes
                          </a>
      <br>&nbsp;
      
    </div>
  </div>
</div>

<script type="text/javascript">
      
      $(document).ready(function(){
               
                 $('#avatar-modal').on('hidden.bs.modal', function () {
                    location.reload();
                });
                 $('#avatar-modal-btn-close').on('click', function () {
                      location.reload();
                });

                 

            });

    </script>

</body>
</html>