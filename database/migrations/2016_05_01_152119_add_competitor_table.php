<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompetitorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('dni',20);
            $table->string('phone',20);
            $table->date('birth_date');
            $table->string('department',254);
            $table->string('city',254);
            $table->string('email')->unique();
            $table->string('source');
            $table->string('photo_id');
            $table->string('description',140);
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('competitor');
    }
}
