<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(MasterTableSeeder::class);
        $this->call(CompetitionTableSeeder::class);
        $this->call(UserTableSeeder::class);
        
    }
}
