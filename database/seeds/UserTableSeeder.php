<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
        	['name'=>'Puntodos', 'email'=>'info@puntodos.co', 'password'=>bcrypt('p2jdhgu2832')],
            ['name'=>'LaFotoMillonaria', 'email'=>'lafotomillonaria@florhuila.com', 'password'=>bcrypt('lfMeKg82327g-kPjG3')]
        ];
        foreach ($data as $value) {
            App\User::create($value);
        }
    }
}
