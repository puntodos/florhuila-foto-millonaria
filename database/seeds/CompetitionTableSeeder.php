<?php

use Illuminate\Database\Seeder;

class CompetitionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

/*

*Primera Semana 16 de Mayo 2016 desde las 00:00 - 02 de Junio hasta las 8:00 AM
*Segunda Semana 03 de Junio 2016 desde las 00:00 - 09 de Junio hasta las 8:00 AM
*Tercera Semana 10 de Junio 2016 desde las 00:00 - 16 de Junio hasta las 8:00 AM
*Cuarta Semana 17 de Junio 2016 desde las 00:00 - 23 de Junio hasta las 8:00 AM
*Quinta Semana 24 de Junio 2016 desde las 00:00 - 30 de Junio hasta las 8:00 AM
*Sexta Semana 01 de Julio 2016 desde las 00:00 - 07 de Julio hasta las 00:00 PM

*/

    	$data = [
    	['id'=>'1','name'=>'Semana 1','date_init'=>Carbon\Carbon::create(2016, 05, 16, 0, 0, 1),'date_end'=>Carbon\Carbon::create(2016, 06, 2, 8, 0, 0)],
    	['id'=>'2','name'=>'Semana 2','date_init'=>Carbon\Carbon::create(2016, 06, 2, 8, 0, 1),'date_end'=>Carbon\Carbon::create(2016, 06, 9, 8, 0, 0)],
        ['id'=>'3','name'=>'Semana 3','date_init'=>Carbon\Carbon::create(2016, 06, 9, 8, 0, 1),'date_end'=>Carbon\Carbon::create(2016, 06, 16, 8, 0, 0)],
        ['id'=>'4','name'=>'Semana 4','date_init'=>Carbon\Carbon::create(2016, 06, 16, 8, 0, 1),'date_end'=>Carbon\Carbon::create(2016, 06, 23, 8, 0, 0)],
        ['id'=>'5','name'=>'Semana 5','date_init'=>Carbon\Carbon::create(2016, 06, 23, 8, 0, 1),'date_end'=>Carbon\Carbon::create(2016, 06, 30, 8, 0, 0)],
        ['id'=>'6','name'=>'Semana 6','date_init'=>Carbon\Carbon::create(2016, 06, 30, 8, 0, 1),'date_end'=>Carbon\Carbon::create(2016, 07, 7, 21, 59, 59)]

    	];
        foreach ($data as $value) {
            App\Competition::create($value);
        }
    }
}
