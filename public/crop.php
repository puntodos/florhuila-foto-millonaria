<?php
class CropAvatar {
  private $src;
  private $data;
  private $dst;
  private $type;
  private $extension;
  private $msg;
  public $name;
  public $file_name;

  function __construct($src, $data, $file) {
    $this -> name = date('YmdHis') . rand(0,1000000);
    $this -> setSrc($src);
    $this -> setData($data);
    $this -> setFile($file);
    $this -> crop($this -> src, $this -> dst, $this -> data);
    $this -> deleteImage($file);
  }

  private function setSrc($src) {
    if (!empty($src)) {
      $type = exif_imagetype($src);

      if ($type) {
        $this -> src = $src;
        $this -> type = $type;
        $this -> extension = image_type_to_extension($type);
        $this -> setDst();
      }
    }
  }

  private function setData($data) {
    if (!empty($data)) {
      $this -> data = json_decode(stripslashes($data));
    }
  }

  private function setFile($file) {
    $errorCode = $file['error'];

    if ($errorCode === UPLOAD_ERR_OK) {
      $type = exif_imagetype($file['tmp_name']);

      if ($type) {
        $extension = image_type_to_extension($type);
        $src = 'tmp/' . $this -> name . '.original' . $extension;

        if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_JPEG || $type == IMAGETYPE_PNG) {

          if (file_exists($src)) {
            unlink($src);
          }

          $result = move_uploaded_file($file['tmp_name'], $src);

          if ($result) {
            $this -> src = $src;
            $this -> type = $type;
            $this -> extension = $extension;
            $this -> setDst();
          } else {
             $this -> msg = 'No pudo ser guardada la imagen';
          }
        } else {
          $this -> msg = 'Por favor subir la imagen en alguno de los siguiente formatos: JPG, PNG, GIF';
        }
      } else {
        $this -> msg = 'Por favor suba un archivo de imagen';
      }
    } else {
      $this -> msg = $this -> codeToMessage($errorCode);
    }
  }

  private function setDst() {
    $this -> file_name = $this -> name . '.jpeg';
    $this -> dst = 'tmp/' . $this -> file_name;
  }

  private function crop($src, $dst, $data) {
    if (!empty($src) && !empty($dst) && !empty($data)) {
      switch ($this -> type) {
        case IMAGETYPE_GIF:
          $src_img = imagecreatefromgif($src);
          break;

        case IMAGETYPE_JPEG:
          $src_img = imagecreatefromjpeg($src);
          break;

        case IMAGETYPE_PNG:
          $src_img = imagecreatefrompng($src);
          break;
      }

      if (!$src_img) {
        $this -> msg = "No pudo ser leida la imagen";
        return;
      }

      $size = getimagesize($src);
      $size_w = $size[0]; // natural width
      $size_h = $size[1]; // natural height

      $src_img_w = $size_w;
      $src_img_h = $size_h;

      $degrees = $data -> rotate;

      // Rotate the source image
      if (is_numeric($degrees) && $degrees != 0) {
        // PHP's degrees is opposite to CSS's degrees
        $new_img = imagerotate( $src_img, -$degrees, imagecolorallocatealpha($src_img, 0, 0, 0, 127) );
        

        imagedestroy($src_img);
        $src_img = $new_img;

        $deg = abs($degrees) % 180;
        $arc = ($deg > 90 ? (180 - $deg) : $deg) * M_PI / 180;

        $src_img_w = $size_w * cos($arc) + $size_h * sin($arc);
        $src_img_h = $size_w * sin($arc) + $size_h * cos($arc);

        // Fix rotated image miss 1px issue when degrees < 0
        $src_img_w -= 1;
        $src_img_h -= 1;
      }

      $tmp_img_w = $data -> width;
      $tmp_img_h = $data -> height;
      $dst_img_w = 940;
      $dst_img_h = 510;

      $src_x = $data -> x;
      $src_y = $data -> y;

      if ($src_x <= -$tmp_img_w || $src_x > $src_img_w) {
        $src_x = $src_w = $dst_x = $dst_w = 0;
      } else if ($src_x <= 0) {
        $dst_x = -$src_x;
        $src_x = 0;
        $src_w = $dst_w = min($src_img_w, $tmp_img_w + $src_x);
      } else if ($src_x <= $src_img_w) {
        $dst_x = 0;
        $src_w = $dst_w = min($tmp_img_w, $src_img_w - $src_x);
      }

      if ($src_w <= 0 || $src_y <= -$tmp_img_h || $src_y > $src_img_h) {
        $src_y = $src_h = $dst_y = $dst_h = 0;
      } else if ($src_y <= 0) {
        $dst_y = -$src_y;
        $src_y = 0;
        $src_h = $dst_h = min($src_img_h, $tmp_img_h + $src_y);
      } else if ($src_y <= $src_img_h) {
        $dst_y = 0;
        $src_h = $dst_h = min($tmp_img_h, $src_img_h - $src_y);
      }

      // Scale to destination position and size
      $ratio = $tmp_img_w / $dst_img_w;
      $dst_x /= $ratio;
      $dst_y /= $ratio;
      $dst_w /= $ratio;
      $dst_h /= $ratio;

      $dst_img = imagecreatetruecolor($dst_img_w, $dst_img_h);

      // Add transparent background to destination image
      //imagefill($dst_img, 0, 0, imagecolorallocatealpha($dst_img, 0, 0, 0, 127));
      //imagesavealpha($dst_img, true);

      $result = imagecopyresampled($dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);

      if ($result) {
        $this->marcaDeAguaMarcoPaginaWeb($dst_img);
        //if (!imagepng($dst_img, $dst)) {
        if (!imagejpeg($dst_img, $dst)) {
          $this -> msg = "No se pudo guardar la imgen editada";
        }
      } else {
        $this -> msg = "No se pudo guardar la imagen";
      }

    }
  }

  function marcaDeAguaMarcoPaginaWeb($im){

  $width = 940;
  $height = 510;
  $stamp = imagecreatefrompng('assets/images/watermark.png');

  $nuevoAncho = 940;
  //echo 'datos:' . $ancho . ':' . $alto . '<br>';

  $sx = imagesx($stamp);
  $sy = imagesy($stamp);
  
  $thumb = imagecreatetruecolor($width, $height);
  imagesavealpha($thumb, true);

    $trans_colour = imagecolorallocatealpha($thumb, 0, 0, 0, 127);
    imagefill($thumb, 0, 0, $trans_colour);

  imagecopyresized($thumb, $stamp, 0, 0, 0, 0, $width, $height, $sx, $sy);
  $stamp = $thumb;
  //$stamp = redimensionarImagen($stamp, $width, $height);

  // Set the margins for the stamp and get the height/width of the stamp image
  $marge_right = 0;
  $marge_bottom = 0;
  $sx = imagesx($stamp);
  $sy = imagesy($stamp);

  // Copy the stamp image onto our photo using the margin offsets and the photo 
  // width to calculate positioning of the stamp. 
  imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
  
}

  private function codeToMessage($code) {
    $errors = array(
      UPLOAD_ERR_INI_SIZE =>'El tamaño del archivo que se está subiendo excede el limite permitido',
      UPLOAD_ERR_FORM_SIZE =>'El tamaño del Archivo que se está subiendo excede el limite permitido',
      UPLOAD_ERR_PARTIAL =>'El archivo fue cargado parcialmente',
      UPLOAD_ERR_NO_FILE =>'No pudo ser cargado',
      UPLOAD_ERR_NO_TMP_DIR =>'No se encuentra la carpeta',
      UPLOAD_ERR_CANT_WRITE =>'No se puedo escribir en disco',
      UPLOAD_ERR_EXTENSION =>'Se detuvo la carga del archivo por la extensión',
    );

    if (array_key_exists($code, $errors)) {
      return $errors[$code];
    }

    return 'Error de carga desconocido';
  }

  public function getResult() {
    return !empty($this -> data) ? $this -> dst : $this -> src;
  }

  public function getMsg() {
    return $this -> msg;
  }

  public function deleteImage($file){
    if (file_exists($this -> src)) {
      unlink($this -> src);
    }
  }
}

$crop = new CropAvatar(
  isset($_POST['avatar_src']) ? $_POST['avatar_src'] : null,
  isset($_POST['avatar_data']) ? $_POST['avatar_data'] : null,
  isset($_FILES['avatar_file']) ? $_FILES['avatar_file'] : null
);

$response = array(
  'state'  => 200,
  'message' => $crop -> getMsg(),
  'result' => $crop -> getResult(),
  'photo_id' => $crop -> file_name
);

echo json_encode($response);
