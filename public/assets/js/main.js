var filter = "";
var page = "";
(function (factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as anonymous module.
    define(['jquery'], factory);
  } else if (typeof exports === 'object') {
    // Node / CommonJS
    factory(require('jquery'));
  } else {
    // Browser globals.
    factory(jQuery);
  }
})(function ($) {

  'use strict';

  var console = window.console || { log: function () {} };

  function CropAvatar($element) {
    this.$container = $element;

    //this.$avatarView = this.$container.find('.avatar-view');
    this.$avatarView = $('.avatar-view');
    this.$avatar = this.$avatarView.find('img');
    this.$avatarModal = this.$container.find('#avatar-modal');
    //this.$loading = this.$container.find('.loading');


    this.$avatarForm = this.$avatarModal.find('.avatar-form');
    this.$avatarUpload = this.$avatarForm.find('.avatar-upload');
    this.$avatarSrc = this.$avatarForm.find('.avatar-src');
    this.$avatarData = this.$avatarForm.find('.avatar-data');
    this.$avatarInput = this.$avatarForm.find('.avatar-input');
    this.$avatarSave = this.$avatarForm.find('.avatar-save');
    this.$avatarBtns = this.$avatarForm.find('.avatar-btns');

    this.$avatarWrapper = this.$avatarModal.find('.avatar-wrapper');
    this.$avatarPreview = this.$avatarModal.find('.avatar-preview');

    this.init();
  }

  CropAvatar.prototype = {
    constructor: CropAvatar,

    support: {
      fileList: !!$('<input type="file">').prop('files'),
      blobURLs: !!window.URL && URL.createObjectURL,
      formData: !!window.FormData
    },

    init: function () {
      this.support.datauri = this.support.fileList && this.support.blobURLs;

      if (!this.support.formData) {
        this.initIframe();
      }

      this.initTooltip();
      this.initModal();
      this.addListener();
    },

    addListener: function () {
      this.$avatarView.on('click', $.proxy(this.click, this));
      this.$avatarInput.on('change', $.proxy(this.change, this));
      this.$avatarForm.on('submit', $.proxy(this.submit, this));
      this.$avatarBtns.on('click', $.proxy(this.rotate, this));
    },

    initTooltip: function () {
      this.$avatarView.tooltip({
        placement: 'bottom'
      });
    },

    initModal: function () {
      this.$avatarModal.modal({
        show: false
      });
    },

    initPreview: function () {
      var url = this.$avatar.attr('src');

      this.$avatarPreview.html('<img src="' + url + '">');
      $("#alert-p2").hide();
    },

    initIframe: function () {
      var target = 'upload-iframe-' + (new Date()).getTime();
      var $iframe = $('<iframe>').attr({
            name: target,
            src: ''
          });
      var _this = this;

      // Ready ifrmae
      $iframe.one('load', function () {

        // respond response
        $iframe.on('load', function () {
          var data;

          try {
            data = $(this).contents().find('body').text();
          } catch (e) {
            console.log(e.message);
          }

          if (data) {
            try {
              data = $.parseJSON(data);
            } catch (e) {
              console.log(e.message);
            }

            _this.submitDone(data);
          } else {
            _this.submitFail('Image upload failed!');
          }

          _this.submitEnd();

        });
      });

      this.$iframe = $iframe;
      this.$avatarForm.attr('target', target).after($iframe.hide());
    },

    click: function () {
      this.$avatarModal.modal('show');
      this.initPreview();
    },

    change: function () {
      var files;
      var file;
      
      //$("#id-loading").removeAttr('style');

      if (this.support.datauri) {
        files = this.$avatarInput.prop('files');

        if (files.length > 0) {
          file = files[0];

          if (this.isImageFile(file)) {
            if (this.url) {
              URL.revokeObjectURL(this.url); // Revoke the old one
            }

            this.url = URL.createObjectURL(file);
            this.startCropper();
          }
        }
      } else {
        file = this.$avatarInput.val();

        if (this.isImageFile(file)) {
          this.syncUpload();
        }
      }
      //$("#id-loading").css("display","none");
    },

    submit: function () {
      if (!this.$avatarSrc.val() && !this.$avatarInput.val()) {
        var _this = this;
        _this.submitFail("No se ha seleccionado ninguna imagen");
        return false;
      }

      if (this.support.formData) {
        this.ajaxUpload();
        return false;
      }
    },

    rotate: function (e) {
      var data;
      
      if (this.active) {
        data = $(e.target).data();

        if (data.method) {
          if(data.invader)
          {
            this.$img.cropper(data.invader, data.option);
          }else{
            this.$img.cropper(data.method, data.option);
          }
        }
      }
    },

    isImageFile: function (file) {
      if (file.type) {
        return /^image\/\w+$/.test(file.type);
      } else {
        return /\.(jpg|jpeg|png|gif)$/.test(file);
      }
    },

    startCropper: function () {
      var _this = this;

      if (this.active) {
        this.$img.cropper('replace', this.url);
      } else {
        this.$img = $('<img src="' + this.url + '">');
        this.$avatarWrapper.empty().html(this.$img);
        this.$img.cropper({
          aspectRatio: 470 / 255,
          preview: '.img-preview',
          viewMode: 1,
          dragMode: 'move',
          restore: false,
          guides: false,
          highlight: false,
          cropBoxMovable: false,
          cropBoxResizable: false,
          zoomable:true,
          preview: this.$avatarPreview.selector,
          crop: function (e) {
            var json = [
                  '{"x":' + e.x,
                  '"y":' + e.y,
                  '"height":' + e.height,
                  '"width":' + e.width,
                  '"rotate":' + e.rotate + '}'
                ].join();

            _this.$avatarData.val(json);
          }
        });

        this.active = true;
      }

      this.$avatarModal.one('hidden.bs.modal', function () {
        _this.$avatarPreview.empty();
        _this.stopCropper();
      });
    },

    stopCropper: function () {
      if (this.active) {
        this.$img.cropper('destroy');
        this.$img.remove();
        this.active = false;
      }
    },

    ajaxUpload: function () {
      waitingDialog.show('Cargando...', {dialogSize: 'sm', progressType: 'danger'});
      var url = this.$avatarForm.attr('action');
      var data = new FormData(this.$avatarForm[0]);
      var _this = this;

      $.ajax(url, {
        type: 'post',
        data: data,
        dataType: 'json',
        processData: false,
        contentType: false,

        beforeSend: function () {
          _this.submitStart();
        },

        success: function (data) {
          _this.submitDone(data);
          waitingDialog.hide();
        },

        error: function (XMLHttpRequest, textStatus, errorThrown) {
          _this.submitFail(textStatus || errorThrown);
          waitingDialog.hide();
        },

        complete: function () {
          _this.submitEnd();
        }
      });

    },

    syncUpload: function () {
      this.$avatarSave.click();
    },

    submitStart: function () {
      //this.$loading.fadeIn();
    },

    submitDone: function (data) {
      console.log(data);

      if ($.isPlainObject(data) && data.state === 200) {
        if (data.result) {
          this.url = data.result;
          console.log(data);
          if (this.support.datauri || this.uploaded) {
            this.uploaded = false;
            this.cropDone();
          } else {
            this.uploaded = true;
            this.$avatarSrc.val(this.url);
            this.startCropper();
          }
          $("#photo_id").val(data.photo_id);
          this.$avatarInput.val('');
        } else if (data.message) {
          this.alert(data.message);
        }
      } else {
        this.alert('Failed to response');
      }
    },

    submitFail: function (msg) {
      this.alert(msg);
    },

    submitEnd: function () {
      //this.$loading.fadeOut();
    },

    cropDone: function () {
      this.$avatarForm.get(0).reset();
      this.$avatar.attr('src', this.url);
      this.stopCropper();
      this.$avatarModal.modal('hide');
    },

    alert: function (msg) {
      var $alert = [
            '<div id="alert-p2" class="alert alert-danger avatar-alert alert-dismissable">',
              '<button type="button" class="close" data-dismiss="alert">&times;</button>',
              msg,
            '</div>'
          ].join('');

      this.$avatarUpload.after($alert);
    }
  };

  $(function () {
    return new CropAvatar($('#crop-avatar'));
  });

});

$(document).ready(function(){
  $("#btn-upload-photo").on("click",function(){
    $(".avatar-view").trigger("click");
  });
  $("#file-upload-p2").on("click",function(){
    $("#avatarInput").trigger("click");
  });
  $("#avatar-view").on("click",function(){
    $("#file-upload-p2").trigger("click");
  });

  $(".glyphicon-calendar").on("click",function(){
    $("#birth_date").trigger("click");
  });
  
  if($("#pagination").length){
    var html_pagination = $("#pagination").html();
    html_pagination = html_pagination.replace("«","Anterior");
    html_pagination = html_pagination.replace("»","Siguiente");
    $("#pagination").html(html_pagination);

    $('#pagination').find("a").each(function () {
      var href = $(this).attr("href")+"&filter="+filter+"#title-participants";
      $(this).attr("href",href);
      /*
      var href = $(this).attr("href");
      $(this).removeAttr("href");
      $(this).css("cursor", "pointer");
      $(this).on("click",function(){
        paginate_grid(href);
      });*/
      
    });
  }

  $("#themost").on("click",function(){
      filter = "most";
      window.location.href = window.location.pathname+"?filter="+filter+"#title-participants";

  });

  $("#thelast").on("click",function(){
      filter = "last";
      window.location.href = window.location.pathname+"?filter="+filter+"#title-participants";
  });

  $("#search").on("click",function(){
      filter = "search";
      var term = $("#search_query").val();
      window.location.href = window.location.pathname+"?filter="+filter+"&q="+term+"#title-participants";
  });
});

var setDefaultButton = function(){
  if(filter == "most"){
    $("#themost").addClass("btn-gray-hover");
    $("#themost").removeClass("btn-gray");
  }
  if(filter == "last"){
    $("#thelast").addClass("btn-gray-hover");
    $("#thelast").removeClass("btn-gray");
  }
}

$(function() {
    moment.locale('es', {
    months : "enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre".split("_"),
    monthsShort : "ene._feb._mar_abr._may_jun_jul._ago_sept._oct._nov._dic.".split("_"),
    weekdays : "domingo_lunes_martes_miercoles_jueves_viernes_sabado".split("_"),
    weekdaysShort : "dom._lun._mar._mie._jue._vie._sab.".split("_"),
    weekdaysMin : "Do_Lu_Ma_Mi_Ju_Vi_Sa".split("_"),
    longDateFormat : {
        LT : "HH:mm",
        LTS : "HH:mm:ss",
        L : "DD/MM/YYYY",
        LL : "D MMMM YYYY",
        LLL : "D MMMM YYYY LT",
        LLLL : "dddd D MMMM YYYY LT"
    },
    calendar : {
        sameDay: "[Aujourd'hui à] LT",
        nextDay: '[Demain à] LT',
        nextWeek: 'dddd [à] LT',
        lastDay: '[Hier à] LT',
        lastWeek: 'dddd [dernier à] LT',
        sameElse: 'L'
    },
    relativeTime : {
        future : "dans %s",
        past : "il y a %s",
        s : "quelques secondes",
        m : "une minute",
        mm : "%d minutes",
        h : "une heure",
        hh : "%d heures",
        d : "un jour",
        dd : "%d jours",
        M : "un mois",
        MM : "%d mois",
        y : "une année",
        yy : "%d années"
    },
    ordinalParse : /\d{1,2}(er|ème)/,
    ordinal : function (number) {
        return number + (number === 1 ? 'er' : 'ème');
    },
    meridiemParse: /PD|MD/,
    isPM: function (input) {
        return input.charAt(0) === 'M';
    },
    // in case the meridiem units are not separated around 12, then implement
    // this function (look at locale/id.js for an example)
    // meridiemHour : function (hour, meridiem) {
    //     return /* 0-23 hour, given meridiem token and hour 1-12 */
    // },
    meridiem : function (hours, minutes, isLower) {
        return hours < 12 ? 'PD' : 'MD';
    },
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
    }
});

    $('input[name="birth_date"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        "minDate": "01/01/1900",
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    }, 
    function(start, end, label) {
        /*var years = moment().diff(start, 'years');
        alert("You are " + years + " years old.");*/
    });

     $('input[name="birth_date"]').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('MM/DD/YYYY'));
      });
});



$("#sendBtn").on("click",function(){
    
    $.ajax({
            url:  '/participar/check',
            type: "post",
            data: $("#participar_form").serialize(),
            error: function(jqXHR, textStatus, errorThrown){
                
                if( jqXHR.status === 422 ) {
                  //process validation errors here.
                  errors = jqXHR.responseJSON; //this will get the errors response data.
                  //show them somewhere in the markup
                  //e.g
                  errorsHtml = '<div class="alert alert-danger"><ul>';

                  $.each( errors, function( key, value ) {
                      errorsHtml += '<li>' + value[0] + '</li>'; //showing only the first error.
                  });
                  errorsHtml += '</ul></di>';
                      
                  $( '#validate_errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
                  } else {
                      /// do some thing else
                  }
                  $('html, body').animate({
                      scrollTop: $("#validate_errors").offset().top
                  }, 2000);
            },
            success: function(data) { 
                if(data.status == 'OK'){
                    $("#participar_form").submit();
                }
                
            }
        });
});


var showFaqModal = function(){
  $('#faq-modal').modal('show');
}

var showInfoModal = function(){
  $('#info-modal').modal('show');
}

var closeFaqModal = function(){
  $('#faq-modal').modal('hide');
}

var closeInfoModal = function(){
  $('#info-modal').modal('hide');
}

var showModalProfile = function(id, image, name, city, department, votes, description, permalink){

  $("#modal_image").attr("src",'/img/'+image);
  $("#modal_name").html(name);
  $("#modal_city").html(city);
  $("#modal_department").html(department);
  $("#participant_votes_cant").html(votes);
  $("#participant_description").html(description);
  $("#participant_id").val(id);
  $("#facebook_share").attr("href", "http://www.facebook.com/sharer.php?u=http://www.florhuila.com/lafotomillonariaflorhuila/"+ permalink);
  $("#facebook_share_2").attr("href", "http://www.facebook.com/sharer.php?u=http://www.florhuila.com/lafotomillonariaflorhuila/"+ permalink);
  
  $('#avatar-modal').modal('show');
  $("#body_tks_profile_modal").hide();
  $('#body_profile_modal').show();
  
  

}


var filter_grid = "";
var paginate_grid = function(url){
  var page = ""
  if(url != ""){
    page = url.split("page=")[1];
  }
    $.ajax({
            url:  '/participants/filter?page='+page+'&filter='+filter_grid+"&action=",
            type: "get",
            error: function(jqXHR, textStatus, errorThrown){
                alert(jqXHR);
            },
            success: function(dataRes) {
               //alert(JSON.stringify(dataRes));
              var html = "";
              for(var i=0; i < dataRes.data.length; i++){
                html += getValueToResultGrid(dataRes.data[i]);
              }
              $("#result_grid").html(html);
              paginate_grid_count(url);
            }
        });
}


var getValueToResultGrid = function(data){
  

  var html = "('"+data.id+"','"+data.photo_id+"','"
    +data.first_name+" "+data.last_name
    +"','"+data.master_city.name+"','"+data.master_department.name
    +"'')";

  html = '<div class="col-md-3 click-style" onclick="showModalProfile'+html+ '">';
  html += '<a><img src="/img/'+data.photo_id+'" class="center-block img-responsive modalImage"></a>';
  html += '<h3 class="grid-text">'+data.first_name+" "+data.last_name+'</h3>';
  html += '<p>';
  html += '<i class="fa fa-heart" aria-hidden="true">&nbsp;';
  html += '<span class="grid-count">'+data.votes+'</span>';
  html += '</i><br>&nbsp;';
  html += '</p></div>';
  return html;    
}


var paginate_grid_count = function(url){
  var page = ""
  if(url != ""){
    page = url.split("page=")[1];
  }
    $.ajax({
            url:  '/participants/filter?page='+page+'&filter='+filter_grid+"&action=count",
            type: "get",
            error: function(jqXHR, textStatus, errorThrown){
                alert(jqXHR);
            },
            success: function(dataRes) {
               alert(dataRes);
               alert(JSON.stringify(dataRes));
              /*var html = "";
              for(var i=0; i < dataRes.data.length; i++){
                html += getValueToResultGrid(dataRes.data[i]);
              }
              $("#result_grid").html(html);*/
            }
        });
}



$(document).ready(function() {
    var text_max = 140;
    $('#description_feed').html('Describe tu foto ('+text_max+'):');

    $('#description').keyup(function() {
        var text_length = $('#description').val().length;
        var text_remaining = text_max - text_length;

        $('#description_feed').html('Describe tu foto ('+text_remaining+'):');
    });


  

});


function scrollToAnchor(aid){
    
    $('html, body').animate({
                      scrollTop: $("#"+aid).offset().top
                  }, 2000);
    
}

var getCities = function(departmentId, cityId){
 
  $.get('/participar/cities/'+departmentId, function(response, state){
      $("#city").empty();
      $("#city").append('<option value="">Seleccione una Ciudad...</option>');  
      var selected = "";
      for(var i=0; i < response.length; i++){
        if(cityId == response[i].id) selected = "selected";
        $("#city").append('<option value="'+response[i].id+'" '+selected+'>'+response[i].name+'</option>')
        selected = "";
      }
    });
}

$("#department").on("change",function(event){
    getCities(event.target.value,'');
  });

$(document).ready(function(){
        getCities($("#old_department").val(),$("#old_city").val());
});


