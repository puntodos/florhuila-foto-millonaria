var clicking = true;
var voted = false;
$(document).ready(function(){
   // $('#avatar-modal').modal('show');

    //check if department list have a value
    
    $("#participant_vote").on("click",function(){
       // alert(clicking);
    	if(clicking){
    		clicking = false; 
    		//alert($("#participant_id").val());
    		$.ajax({
                url:  '/participante/votar/'+$("#participant_id").val(),
                type: "get",
                error: function(jqXHR, textStatus, errorThrown){
                    console.log("AJAX Error: #{textStatus}");
                    console.log("AJAX Error: #{errorThrown}");
                    clicking = true; 
                },
                success: function(data) { 
                    //alert(JSON.stringify(data));
                	if(data.status == 'OK'){
                    	$("#participant_votes_cant").html(data.votes);
                        $('#body_profile_modal').hide();
                        $("#body_tks_profile_modal").show();
                        $("#modal_result_vote").html("¡Gracias por votar por "+$("#modal_name").html()+"!<br>Ayúdale a compartir su receta para que pueda tener más posibilidades de ganar.");
                        $(".modalPicture").css("height","90%");
                        
                        voted = true;
                	}else{
                        $('#body_profile_modal').hide();
                        $("#body_tks_profile_modal").show();
                        $("#modal_result_vote").html(data.message);
                        $(".modalPicture").css("height","90%");
                    }
                    clicking = true; 

                }
            });
    	}
    });

    /*$('#avatar-modal').on('hidden.bs.modal', function () {
        alert("close");
        window.location.href = "/#title-participants";
    });*/

});