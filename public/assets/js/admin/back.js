//var filter = "";
$(function() {
    moment.locale('es', {
    months : "enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre".split("_"),
    monthsShort : "ene._feb._mar_abr._may_jun_jul._ago_sept._oct._nov._dic.".split("_"),
    weekdays : "domingo_lunes_martes_miercoles_jueves_viernes_sabado".split("_"),
    weekdaysShort : "dom._lun._mar._mie._jue._vie._sab.".split("_"),
    weekdaysMin : "Do_Lu_Ma_Mi_Ju_Vi_Sa".split("_"),
    longDateFormat : {
        LT : "HH:mm",
        LTS : "HH:mm:ss",
        L : "DD/MM/YYYY",
        LL : "D MMMM YYYY",
        LLL : "D MMMM YYYY LT",
        LLLL : "dddd D MMMM YYYY LT"
    },
    calendar : {
        sameDay: "[Aujourd'hui à] LT",
        nextDay: '[Demain à] LT',
        nextWeek: 'dddd [à] LT',
        lastDay: '[Hier à] LT',
        lastWeek: 'dddd [dernier à] LT',
        sameElse: 'L'
    },
    relativeTime : {
        future : "dans %s",
        past : "il y a %s",
        s : "quelques secondes",
        m : "une minute",
        mm : "%d minutes",
        h : "une heure",
        hh : "%d heures",
        d : "un jour",
        dd : "%d jours",
        M : "un mois",
        MM : "%d mois",
        y : "une année",
        yy : "%d années"
    },
    ordinalParse : /\d{1,2}(er|ème)/,
    ordinal : function (number) {
        return number + (number === 1 ? 'er' : 'ème');
    },
    meridiemParse: /PD|MD/,
    isPM: function (input) {
        return input.charAt(0) === 'M';
    },
    // in case the meridiem units are not separated around 12, then implement
    // this function (look at locale/id.js for an example)
    // meridiemHour : function (hour, meridiem) {
    //     return /* 0-23 hour, given meridiem token and hour 1-12 */
    // },
    meridiem : function (hours, minutes, isLower) {
        return hours < 12 ? 'PD' : 'MD';
    },
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
    }
});
});

$(document).ready(function(){


    if($("#action").val()=='edit'){
      $('input[name="date_init"]').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          "minDate": "01/01/1900",
          timePicker: true,
          timePickerIncrement: 1,
          "timePickerSeconds": true,
          locale: {
              cancelLabel: 'Limpiar',
              applyLabel: 'Aplicar',
              format: 'YYYY/MM/DD h:mm:s'
          }
      }, 
      function(start, end, label) {
          /*var years = moment().diff(start, 'years');
          alert("You are " + years + " years old.");*/
      });

      $('input[name="date_end"]').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          "minDate": "01/01/1900",
          timePicker: true,
          timePickerIncrement: 1,
          "timePickerSeconds": true,
          locale: {
              cancelLabel: 'Limpiar',
              applyLabel: 'Aplicar',
              format: 'YYYY/MM/DD h:mm:s'
          }
      }, 
      function(start, end, label) {
          /*var years = moment().diff(start, 'years');
          alert("You are " + years + " years old.");*/
      });
    }else{
      $('input[name="date_init"]').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          "minDate": "01/01/1900",
          autoUpdateInput: false,
          timePicker: true,
          timePickerIncrement: 1,
          "timePickerSeconds": true,
          locale: {
              cancelLabel: 'Limpiar',
              applyLabel: 'Aplicar',
              format: 'YYYY/MM/DD h:mm:s'
          }
      }, 
      function(start, end, label) {
          /*var years = moment().diff(start, 'years');
          alert("You are " + years + " years old.");*/
      });

      $('input[name="date_end"]').daterangepicker({
          singleDatePicker: true,
          showDropdowns: true,
          "minDate": "01/01/1900",
          autoUpdateInput: false,
          timePicker: true,
          timePickerIncrement: 1,
          "timePickerSeconds": true,
          locale: {
              cancelLabel: 'Limpiar',
              applyLabel: 'Aplicar',
              format: 'YYYY/MM/DD h:mm:s'
          }
      }, 
      function(start, end, label) {
          /*var years = moment().diff(start, 'years');
          alert("You are " + years + " years old.");*/
      });
    }

     $('input[name="date_init"]').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('YYYY/MM/DD hh:mm:s'));
      });

     $('input[name="date_end"]').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('YYYY/MM/DD hh:mm:s'));
      });

     if($("#pagination").length){
        setTimeout(function(){
          setDefaultButton(filter);
        },1000);
        
      }
});

var setDefaultButton = function(filter){
  var html_pagination = $("#pagination").html();
        html_pagination = html_pagination.replace("«","Anterior");
        html_pagination = html_pagination.replace("»","Siguiente");
        $("#pagination").html(html_pagination);

        $('#pagination').find("a").each(function () {
          var href = $(this).attr("href")+"&filter="+filter;
          $(this).attr("href",href);
          /*
          var href = $(this).attr("href");
          $(this).removeAttr("href");
          $(this).css("cursor", "pointer");
          $(this).on("click",function(){
            paginate_grid(href);
          });*/
          
        });
}

var changeFilter = function(){
  window.location.href = window.location.pathname+"?filter="+$("#filter").val();
}

var changeStatus = function(id){
    var status = $('input:radio[name=status_'+id+']:checked').val();  
    if(status == "A" ){//|| status == "I"){
      if(!confirm("Esta acción enviará una notificación por mail al participante ¿esta seguro que desea realizarla?")){
        $('input:radio[name=status_'+id+']').attr('checked', false);
        return;
      }
    }
    waitingDialog.show('Activando y enviando', {dialogSize: 'sm', progressType: 'danger'});
    $.ajax({
            url:  '/admin/participantes/status/'+id,
            type: "get",
            data: {id: id, status: status},
            dataType: 'json',
            error: function(jqXHR, textStatus, errorThrown){
              waitingDialog.hide();
                alert(JSON.stringify(jqXHR));
            },
            success: function(dataRes) {
               if(dataRes.status == 'OK'){
                  if(dataRes.competitor_status == "A"){
                    $("#status_"+id).prop( "checked", true );  
                    $("#text_status_"+id).html("Activo");
                  }else{
                    $("#status_"+id).prop( "checked", false );
                    $("#text_status_"+id).html("Inactivo");  
                  }
               }else{
                  alert(dataRes.message);
               }
               waitingDialog.hide();
            }
        });
}

var winnerChangeStatus = function(idConcurso, idParticipante){

    $.ajax({
            url:  '/admin/ganadore/status',
            type: "get",
            data: {concurso_id: idConcurso, participante_id: idParticipante},
            dataType: 'json',
            error: function(jqXHR, textStatus, errorThrown){
                alert(jqXHR);
            },
            success: function(dataRes) {
              //alert(JSON.stringify(dataRes));
               if(dataRes.status == 'OK'){
                  if(dataRes.text_status == "Ganador"){
                    //alert("#text_status_"+idParticipante);
                    $("#status_"+idParticipante).prop( "checked", true );  
                    $("#text_status_"+idParticipante).html(dataRes.text_status);
                  }else{
                    $("#status_"+idParticipante).prop( "checked", false );
                    $("#text_status_"+idParticipante).html(dataRes.text_status); 
                  }
               }else{
                  $("#status_"+idParticipante).prop( "checked", false );
                  alert(dataRes.message);
               }
            }
        });
}
