<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competitor extends Model
{
    protected $table = "competitor";
    protected $fillable = ['id','first_name','last_name','dni','phone','birth_date','department','city','email','source','photo_id','description','address','status','permalink','search'];

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }

    public function winners()
    {
        return $this->hasMany('App\Winner');
    }

    public function locks()
    {
        return $this->hasMany('App\Lock');
    }

    public function votesByIdCompetition($idCompetition)
    {
        return $this->hasMany('App\Vote')->where('competition_id', $idCompetition);
    }

    public function votesByIdCompetitionAndCompetitor($idCompetition, $idCompetitor)
    {
        return $this->hasMany('App\Vote')->where('competitor_id', $idCompetitor)
        ->where('competition_id', $idCompetition);
    }

    public function winnerByIdCompetitionAndCompetitor($idCompetition, $idCompetitor)
    {
        return $this->hasMany('App\Winner')->where('competitor_id', $idCompetitor)
        ->where('competition_id', $idCompetition);
    }
    /*public function votes(){
    	return $this->belongsToMany('App\Competition','vote')->withPivot('vote')->withTimestamps();
    }

    public function votes_without_pt()
    {
        return $this->hasMany('App\Vote');
    }

    public function winners(){
    	return $this->belongsToMany('App\Competition','winner')->withPivot('sn_selecconado')->withTimestamps();
    }*/

    public function getBestVotes() {
		  return $this
		    ->belongsToMany('App\Competition','vote')
		    ->withPivot('vote')
		    ->orderBy('vote', 'desc');
	}
}
