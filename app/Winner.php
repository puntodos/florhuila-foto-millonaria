<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    protected $table = "winner";
    protected $fillable = ['id','competition_id','competitor_id'];


    public function competition()
    {
        return $this->belongsTo('App\Competition');
    }

    public function competitor()
    {
        return $this->belongsTo('App\Competitor');
    }
}
