<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Competition extends Model
{
    protected $table = "competition";
    protected $fillable = ['id','name','date_init','date_end'];

    public function votes(){
        return $this->hasMany('App\Vote');
    }

    public function winners(){
        return $this->hasMany('App\Winner');
    }

    public function locks(){
        return $this->hasMany('App\Lock');
    }

    public function getMostVoted()
    {
        return $this->hasMany('App\Vote')->orderBy('vote','desc')->select('competitor_id');
    }

    /*public function votes(){
    	return $this->belongsToMany('App\Competitor','vote')->withPivot('vote')->withTimestamps();
    }

    public function winners(){
    	return $this->belongsToMany('App\Competitor','winner')->withPivot('sn_selecconado')->withTimestamps();
    }

    public function getBestVotes() {
		  return $this
		    ->belongsToMany('App\Competitor','vote')
		    ->withPivot('vote')
		    ->orderBy('vote', 'desc');
	}*/

}
