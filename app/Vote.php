<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $table = "vote";
    protected $fillable = ['id','competition_id','competitor_id','vote'];


    public function competition()
    {
        return $this->belongsTo('App\Competition');
    }

    public function competitor()
    {
        return $this->belongsTo('App\Competitor');
    }
}
