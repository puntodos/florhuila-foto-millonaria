<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lock extends Model
{
    protected $table = "Lock";
    protected $fillable = ['id','competition_id','competitor_id','ip'];

    public function competition()
    {
        return $this->belongsTo('App\Competition');
    }

    public function competitor()
    {
        return $this->belongsTo('App\Competitor');
    }

    
}
