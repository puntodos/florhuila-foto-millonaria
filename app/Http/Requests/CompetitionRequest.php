<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;

class CompetitionRequest extends Request
{
    private $date_init;
    private $date_end ;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        \Validator::extend( 'date_overlap', function ( $attribute, $value, $parameters, $validator ) {
                $fields = [ $attribute => $value ];
                $id  = array_get($validator->getData(), $parameters[1], null);
                $date = Carbon::parse(implode($fields));
                $competition = '';
                if($id == ''){
                    error_log('create');
                    $competition = \DB::table('competition')->where('date_init','<=',$date)->where('date_end','>=',$date)->get();   
                }else{
                    error_log('udpate');
                    error_log($id);
                    $competition = \DB::table('competition')->where('date_init','<=',$date)->where('date_end','>=',$date)->where('id','<>',$id)->get();
                    if(count($competition)==0){
                        return true;
                    }else{
                        return false;
                    }
                }
                 return empty( $competition );
            }, 'el rango de fechas seleccionado se solapaga con otro concurso' );

        return [
            'name'=>'required|min:3|max:254',
            'date_init'=>'required|date_overlap:date_init,id',
            'date_end'=>'required|after:date_init|date_overlap:date_end,id'
        ];
    }
}
