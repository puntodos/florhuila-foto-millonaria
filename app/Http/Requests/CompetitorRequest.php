<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;

class CompetitorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $date = Carbon::now();
        $before = $date->subYears(18)->format('Y-m-d');
       
        //['first_name','last_name','dni','phone','birth_date','department','city','email','source','photo_id','description']
        \Validator::extend( 'date_check', function ( $attribute, $value, $parameters, $validator ) {
            $fields = [ $attribute => $value ];
            $date  = array_get($validator->getData(), $parameters[0], null);
            $year  = array_get($validator->getData(), $parameters[1], null);
            $month  = array_get($validator->getData(), $parameters[2], null);
            $day  = array_get($validator->getData(), $parameters[3], null);
            if($year == "" || $year > 2016 || $year < 1900){
                return false;
            }

            if($month == "" || $month > 12 || $month < 1){
                return false;
            }

            if($day == "" || $day > 31 || $day < 1){
                return false;
            }

            error_log($year.'-'.$month.'-'.$day);
            //$date_check = Carbon::createFromFormat('Y-m-d', $date);
            //error_log($date);
            return true;
        },'El campo fecha de nacimiento no es valido(año, mes, dia)');

        return [
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'dni' => 'required|min:5|max:20|unique:competitor',
            'phone' => 'required|min:7|max:20',
            'birth_date' => 'required|date_check:birth_date,year,month,day|before:' . $before,
            'department' => 'required',
            'city' => 'required',
            'email' => 'email|required|min:10|max:100|unique:competitor',
            'source' => 'required',
            'photo_id' => 'required',
            'description' => 'required|min:20|max:140',
            'address' => 'required|min:10|max:100',
            'terms' => 'required'
        ];
    }

    public function friendly_names(){
        return  [
            'first_name' => 'nombre',
            'last_name' => 'apellido',
            'dni' => 'cédula',
            'phone' => 'teléfono',
            'birth_date' => 'fecha de cumpleaños',
            'department' => 'departamento',
            'city' => 'ciudad',
            'email' => 'email',
            'source' => 'fuente',
            'photo_id' => 'foto',
            'description' => 'descripción'
        ];
    }
}
