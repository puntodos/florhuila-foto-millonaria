<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//MAIN PAGE
Route::get('/', 'ParticipateController@home', [
	'uses' => 'ParticipateController@home',
	'as' => 'competitor.home']);


Route::get('/ganadores', 'ParticipateController@winners', [
	'uses' => 'ParticipateController@winners',
	'as' => 'competitor.winners']);

//REGISTER TO PARTICIPATE PAGE
Route::get('/participar', 'ParticipateController@create',['as' => 'participar']);
Route::post('/participar', 'ParticipateController@store');
Route::post('/participar/check', [
	'uses' => 'ParticipateController@store_validation',
	'as'=>'competitor.store_validation']);


Route::get('/participar/cities/{id}', 'ParticipateController@get_city_by_department');

Route::get('/participante/{id}', [
	'uses' => 'ParticipateController@show',
	'as' => 'competitor.show']);

Route::get('/gracias', [
	'uses' => 'ParticipateController@tks',
	'as' => 'competitor.tks']);

Route::get('/participante/data/{id}', [
	'uses' => 'ParticipateController@get_participate',
	'as' => 'competitor.get_participate']);

Route::get('/participante/votar/{permalink}', [
	'uses' => 'ParticipateController@set_vote',
	'as' => 'competitor.set_vote']);

Route::get('/participants/filter', 'ParticipateController@participants_filter', [
	'uses' => 'ParticipateController@participants_filter',
	'as' => 'competitor.participants_filter']);


//SEGURIDAD
Route::get('/admin/auth/login','Auth\AuthController@getLogin');
Route::post('/admin/auth/login', [
	'uses' => 'Auth\AuthController@postLogin',
	'as' => 'admin.auth.login'
	]);

Route::get('/admin/auth/logout', [
	'uses' => 'Auth\AuthController@getLogout',
	'as' => 'admin.auth.logout'
	]);

Route::group(['prefix'=>'admin', 'middleware'=>'auth'],function(){

	Route::get('dashboard', [
	'uses' => 'DashboardController@index',
	'as' => 'admin.dashboard'
	]);

	//COMPETITION
	Route::get('concurso', [
	'uses' => 'CompetitionController@index',
	'as' => 'admin.competition.index'
	]);

	Route::get('concurso/new', [
	'uses' => 'CompetitionController@create',
	'as' => 'admin.competition.create'
	]);

	Route::post('concurso/new', [
	'uses' => 'CompetitionController@store',
	'as' => 'admin.competition.store'
	]);

	Route::get('concurso/edit/{id}', [
	'uses' => 'CompetitionController@edit',
	'as' => 'admin.competition.edit'
	]);

	Route::post('concurso/edit', [
	'uses' => 'CompetitionController@update',
	'as' => 'admin.competition.update'
	]);
	//COMPETITOR
	Route::get('participantes', [
	'uses' => 'CompetitorController@index',
	'as' => 'admin.competitor.index'
	]);

	Route::get('participantes/status/{id}', [
	'uses' => 'CompetitorController@change_status',
	'as' => 'admin.competitor.change_status']);

	Route::get('ganadores/{id}', [
	'uses' => 'WinnersController@show',
	'as' => 'admin.winners.show'
	]);

	Route::get('exportar/{competition}', [
	'uses' => 'CompetitorController@export',
	'as' => 'admin.competitor.export'
	]);

	Route::get('ganadore/status', [
	'uses' => 'WinnersController@change_status',
	'as' => 'admin.winners.change_status']);

});
