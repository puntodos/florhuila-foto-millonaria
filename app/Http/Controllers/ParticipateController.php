<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\View\View;
use App\Http\Requests\CompetitorRequest;
use App\Master;
use App\Vote;
use App\Lock;
use App\Competitor;
use App\Competition;
use Carbon\Carbon;
use Log;
use Validator;



class ParticipateController extends Controller
{

    public function home(Request $request){

        

        $date_end = Carbon::now();
        $filter = $request["filter"];
        $search = $request["q"];

        $competitors = ParticipateController::get_data_paginate_data($date_end,$filter,$search);
        
        $competitions = Competition::where('date_init', '<=', Carbon::now())
                        ->where('date_end', '>=', Carbon::now())
                        ->get();

        //dd($competitors);
        if(!$competitions->isEmpty()){
            foreach ($competitions as $_competition) {
                $date_end = $_competition->date_end;
            }
        }else{
            $competitors = [];
        }

        $date = Carbon::now();
         $competitions_end = Competition::orderBy('id', 'asc')->where('date_end','<',$date)->count();

         //data for piriod buttons
         $competitions_names = Competition::orderBy('id', 'asc')->get();
         foreach($competitions_names as $competitions_name){
            if($competitions_name->date_init  <= $date 
                && $date <= $competitions_name->date_end){
                $competitions_name->current = true;
            }else{
                $competitions_name->current = false;
            }
         }

         
        
        return view('home', ['date_end'=>$date_end,'competitors'=>$competitors,'filter'=>$filter,'competitions_end'=>$competitions_end, 'competitions_names'=>$competitions_names]);
    }

    public function participants_filter(Request $request){

        $date = Carbon::now();
        $competitors = ParticipateController::get_data_paginate_data($date,$request["filter"]);
        
        if($request["action"] == ""){
            return response()->json($competitors);
        }else{
            error_log(view('layaout.paginate', array('competitors' => $competitors))->render());
            return response()->json("voy");
        }
        
    }

    public function create(Request $request){
    	
    	$departments = Master::where('group','DEPARTAMENTO')->orderBy('name', 'asc')
    		->get()->lists('name','id');
        //detectamos si hay algo en el request
        //dd($request->all());    
    	
    	return view('participate.create', ['departments'=>$departments,'cities'=>[],'city'=>'']);
    }

    public function store(CompetitorRequest $request){
    	//dd($request->all());
        error_log("store");
    	$competitor = new Competitor($request->all());
        //error_reporting('fecha validando '.$request->birth_date);
    	$competitor->birth_date = date('Y-m-d', strtotime($request->birth_date));

        $competitor->permalink = $competitor->first_name .'-'.$competitor->last_name .'-'.rand(1000000,9999999);
        $competitor->permalink = str_replace(' ', '-', $competitor->permalink);
        $competitor->permalink = strtolower($competitor->permalink);
        $competitor->permalink = preg_replace('/[^A-Za-z0-9\-]/', '', $competitor->permalink);
        
        $first_name = strtolower($competitor->first_name);
        $last_name = strtolower($competitor->last_name);

        $first_name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $first_name);
        $last_name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $last_name);

        $competitor->search = $first_name .' '.$last_name;

        //se recuperan los competition actuales
        if($competitor->save()){
            $competitions = Competition::all();

            //se agregan los registros pivote 
            //dd($competitor);
            foreach ($competitions as $competition) {
                $vote = new Vote();
                $vote->competition_id = $competition->id;
                $vote->competitor_id  = $competitor->id; 
                $vote->vote=0;
                $vote->save();
            }

    	   
    		  copy('tmp/'.$competitor -> photo_id, 'img/'.$competitor -> photo_id);
    		  return redirect()->route('competitor.tks');
    	}else{

    	}
    }

    public function tks(){
        
        return view("participate.tks");
    }

    public function store_validation(CompetitorRequest $request){
        return response()->json(['status' => 'OK', 'message' => 'No existe el participante']);
    }

    public function show(Request $request, $id){
        error_log("show");
    	$competitor = Competitor::find($id);
    	$competitor->master_city = Master::find($competitor->city);
    	$competitor->master_department = Master::find($competitor->department);
    	
        /*$competitor -> master_city -> name = ucfirst(strtolower($competitor -> master_city -> name));
        $competitor -> master_department -> name = ucfirst(strtolower($competitor -> master_department -> name));*/
        $competitor -> master_city -> name = strtoupper($competitor -> master_city -> name);
        $competitor -> master_department -> name = strtoupper($competitor -> master_department -> name);

        $competitor -> description = ucfirst(strtolower($competitor -> description));
        $competitor -> description = preg_replace( "/\r|\n/", "",($competitor -> description));

        //buscamos la competencia activa en el momento
        $competitions = Competition::where('date_init', '<=', Carbon::now())
                        ->where('date_end', '>=', Carbon::now())
                        ->get();
        if($competitions->isEmpty()){
            $competitor->cant_votes = 0;
        }else{
            $competition = '';
            foreach ($competitions as $_competition) {
                $competition = $_competition;
            }
            $vote = $competitor->votesByIdCompetition($competition->id)->get();
            
            if($vote->count() == 0){
                $competitor->cant_votes = 0;
            }else{
                $competitor->cant_votes = $vote[0]->vote;
            }
        }
        
        
    	//dd($competitor);
    	return view("participate.show",['competitor'=>$competitor]);
    }

    public function get_city_by_department(Request $request, $id_father){
    	if($request->ajax()){
    		$cities = Master::get_master_by_group_and_id('MUNICIPIO',$id_father);
    		return response()->json($cities);
    	}
    	
    }

    public function lockByIp($ip, $competition_id, $competitor_id){
        //error_log($ip.'::'.$competition_id.'::'.$competitor_id);

        $cantidad = 0;
        $cantidad = \DB::table('Lock')->where('competition_id','=',$competition_id)->where('competitor_id','=',$competitor_id)->where('ip','=',$ip)->count();
        if($cantidad != 0){
            return false;
        }
        $lock = new Lock();
        $lock->competition_id = $competition_id;
        $lock->competitor_id = $competitor_id;
        $lock->ip = $ip;

        if($lock->save()){
           return true;
        }
        return false;
    }

    public function set_vote(Request $request, $id){
        $ip = $_SERVER['REMOTE_ADDR'];
        //es obligatorio que envien el $id del participante y que exista
        //primero validamos la existencia del participante
        $competitor = Competitor::find($id);
        if($competitor === null){
            return response()->json(['status' => 'ER', 'message' => 'No existe el participante']);
        }
        if($competitor->status!="A"){
            return response()->json(['status' => 'ER', 'message' => 'El participante no se encuentra activo']);
        }

        //segundo buscamos la competicion que este activa en el momento
        $competitions = Competition::where('date_init', '<=', Carbon::now())
                        ->where('date_end', '>=', Carbon::now())
                        ->get();
        if($competitions->isEmpty()){
            return response()->json(['status' => 'ER', 'message' => 'No hay un concurso vigente en este momento']);
        }
        //tercero se valida si existe el voto
        $competition = '';
        foreach ($competitions as $_competition) {
            $competition = $_competition;
        }

        /*antes de marcar el voto, hacemos validación de existencia de ip
        si no existe la registramos y continuamos, si existe, sacamos mensaje de 
        error*/
        if(!$this->lockByIp($ip, $competition->id, $competitor->id)){
            return response()->json(['status' => 'ER', 'message' => 'Por el momento solo puedes votar por este participante una vez desde este dispositivo durante esta semana. Para que consigas más votos, te invitamos a que compartas esta foto en Facebook.']); 
        }

        $vote = $competitor->votesByIdCompetition($competition->id)->get();
            
        if($vote->count() == 0){
            //creamos el voto
             $vote = new Vote();
            $vote->competition_id = $competition->id;
            $vote->competitor_id  = $competitor->id; 
            $vote->vote=1;
            if($vote->save()){
               return response()->json(['status' => 'OK', 'message' => 'Tu voto ha sido registrado con éxito']); 
            }
        }else if($vote->count() == 1){
            $vote[0]->vote = $vote[0]->vote + 1;
            if($vote[0]->push()){
                return response()->json(['status' => 'OK', 'message' => 'Tu voto ha sido registrado con éxito']);
            }
        }else{
            return response()->json(['status' => 'ER', 'message' => 'Participante bloqueado para votar']);
        }
        
        return response()->json(['status' => 'ER', 'message' => 'problemas temporales']);
    }

    public function move_image($photo_id){

    }

    public static function get_data_paginate_data($date, $filter = '', $search = ''){
        
        $masterRegxPage = Master::where('group','REGISTROSXPAGINA')->first(); 

        $competitions = Competition::where('date_init', '<=', Carbon::now())
                        ->where('date_end', '>=', Carbon::now())
                        ->get();
        $curr_competition_id = '';
        if(!$competitions->isEmpty()){
            $competition = '';
            foreach ($competitions as $_competition) {
                $competition = $_competition;
            }
            if($competition != ''){
                $date = $competition -> date_end;
                $curr_competition_id = $competition -> id;
            }
        }
        //REALIZAMOS QUERIES Y FILTROS
        error_log("filter".$filter);
        $competitors = [];
        if($filter == ''){
            $competitors = Competitor::orderBy(\DB::raw('RAND()'))->where('status','=','A')->paginate($masterRegxPage->name);//->get();
        }
        if($filter=='last'){
            $competitors = Competitor::orderBy('id', 'desc')->where('status','=','A')->paginate($masterRegxPage->name);
        }
        if($filter=='most'){
            error_log("most voted");
            $competitors = \DB::table('competitor')->where('competitor.status','=','A')->join('vote', 'competitor.id', '=', 'vote.competitor_id')->join('competition', 'vote.competition_id', '=', 'competition.id')->where('vote.competition_id','=',$curr_competition_id)->orderby('vote.vote','desc')->paginate($masterRegxPage->name);
           /*$competitors = \DB::table('competitor')->join('vote', 'competitor.id', '=', 'vote.competitor_id')->join('competition', 'vote.competitor_id', '=', 'competition.id')->where('vote.competition_id','=','1')->orderby('vote.vote','desc')->paginate($masterRegxPage->name);*/
        }
        if($filter == 'search'){
             $search = strtolower($search);
             $search = preg_replace('/[^A-Za-z0-9\-]/', ' ', $search);

             $competitors = \DB::table('competitor')->where('competitor.status','=','A')->where('competitor.search','like','%'.$search.'%')->join('vote', 'competitor.id', '=', 'vote.competitor_id')->join('competition', 'vote.competition_id', '=', 'competition.id')->where('vote.competition_id','=',$curr_competition_id)->orderby('vote.vote','desc')->paginate($masterRegxPage->name);
             

        }
        //dd($competitors);
        if(count($competitors) == 0){
            return $competitors;
        }

        //PROCESAMOS CADA COMPETIDOR
        //dd($competitors);
        foreach ($competitors as $competitor) {
            if($filter == "last" || $filter == ""){
                $competitor->competitor_id = $competitor->id;

            }

            $competitor->votes = '0';
            if($filter=='most' || $filter=='search'){
                $competitor->votes = $competitor->vote;
            }else{
                $vote = $competitor->votesByIdCompetitionAndCompetitor($curr_competition_id, $competitor->id)->get();
                
                if($vote->count() == 1){
                    $competitor->votes = $vote[0]->vote;
                }else{
                    $competitor->votes = '0';
                }
            }
            
            $competitor->master_city = Master::find($competitor->city);
            $competitor->master_department = Master::find($competitor->department);
            //dd($competitor);
            /*$competitor -> master_city -> name = ucfirst(strtolower($competitor -> master_city -> name));
            $competitor -> master_department -> name = ucfirst(strtolower($competitor -> master_department -> name));*/
            $competitor -> master_city -> name = strtoupper($competitor -> master_city -> name);
            $competitor -> master_department -> name = strtoupper($competitor -> master_department -> name);
            
            $competitor -> first_name = ucwords(strtolower($competitor -> first_name));
            $competitor -> last_name = ucwords(strtolower($competitor -> last_name));
            $competitor -> description = ucfirst(strtolower($competitor -> description));
            $competitor -> description = preg_replace( "/\r|\n/", "",($competitor -> description));
            
        }
        //dd($competitors); 
        return $competitors;
    }

    public function get_participate($permalink = ''){

        if($permalink == ""){
            return response()->json(['status' => 'ER', 'message' => 'No se recibió el permalink par ser consultado']);
        }
        $cantidad = 0;
        $cantidad = \DB::table('competitor')->where('permalink','=',$permalink)->where('status','=','A')->count();
        
        if($cantidad == 0){
            return response()->json(['status' => 'ER', 'message' => 'No existe el participante']);
        } 
        $competitor = Competitor::where('permalink','=',$permalink)->select('photo_id as image','permalink as permalink', 'first_name as name', 'last_name as name2','id')->get(); 
        
        //dd($competitor); 
        return response()->json(['status' => 'OK', 'message' => 'Encontrado con éxito','participante'=>$competitor]);     
    }

    public function winners(Request $request){
        $filter = $request["filter"];
        
        //competitions ended
        $competitions = Competition::where('date_end', '<', Carbon::now())->orderBy('date_end', 'asc')->get();
        if($competitions->isEmpty()){
            return redirect()->route('competitor.home');
        }
        if($filter == "" || !is_numeric($filter) || count($competitions) < $filter){
            $filter = 1; 
        }
        //valor de selected par el boton
        $index = 1;
        $idCompetition = 0;
        foreach ($competitions as $competition){
            if($index == $filter){
                $competition->btn_selected = true;  
                $idCompetition = $competition->id;  
            }else{
                $competition->btn_selected = false;
            }
            
            setlocale(LC_TIME, 'es_ES.UTF-8');
            $date = Carbon::parse($competition->date_end);
            $competition->format_date = ucwords($date->formatLocalized('%d %B'));
            $competition->index = $index;  
            $index++;

        }

        //competitors by filter
        $competitors = \DB::table('competitor')->join('winner', 'competitor.id', '=', 'winner.competitor_id')->where('winner.competition_id','=',$idCompetition)->get();
        
        foreach ($competitors as $competitor) {
            
            $vote = \DB::table('vote')->where('competition_id','=',$idCompetition)->where('competitor_id','=',$competitor->id)->get();
            
            if(count($vote) == 1){
                $competitor->votes = $vote[0]->vote;
            }else{
                $competitor->votes = '0';
            }
            
            //error_log('voy');
            $competitor->master_city = Master::find($competitor->city);
            $competitor->master_department = Master::find($competitor->department);
            //dd($competitor);
            /*$competitor -> master_city -> name = ucfirst(strtolower($competitor -> master_city -> name));
            $competitor -> master_department -> name = ucfirst(strtolower($competitor -> master_department -> name));*/
            $competitor -> master_city -> name = strtoupper($competitor -> master_city -> name);
            $competitor -> master_department -> name = strtoupper($competitor -> master_department -> name);

            
            $competitor -> first_name = ucwords(strtolower($competitor -> first_name));
            $competitor -> last_name = ucwords(strtolower($competitor -> last_name));
            $competitor -> description = ucfirst(strtolower($competitor -> description));
            $competitor -> description = preg_replace( "/\r|\n/", "",($competitor -> description));
        }

        return view('ganadores.ganadores', ['competitors'=>$competitors,'filter'=>$filter,'competitions'=>$competitions]);
    }



}
