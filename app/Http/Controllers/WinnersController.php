<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Carbon\Carbon;
use App\Master;
use App\Competitor;
use App\Competition;
use App\Winner;

class WinnersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = Carbon::now();
        $masterRegxPage = Master::where('group','REGISTROSXPAGINA')->first(); 

         $competitions = Competition::orderBy('id', 'asc')->where('date_end','<',$date)->paginate(30);

        $competitions_all = Competition::all();
        return view("admin.winners.index",['competitions'=>$competitions,'competitions_all'=>$competitions_all]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $date = Carbon::now();
        
        $competitions = Competition::orderBy('id', 'asc')->where('date_end','<',$date)->get();


         $competitors = WinnersController::get_data_paginate_data($id);

        $competitions_all = Competition::all();
        return view("admin.winners.show",['competitions'=>$competitions,'competitors'=>$competitors,'competitions_all'=>$competitions_all]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function change_status(Request $request){

        
        $idCompetition = $request["concurso_id"];
        $idCompetitor = $request["participante_id"];
        

        //validamos el concurso cuantos registros tiene de ganadores y lo comparamos con el valor de control

        $masterSelected = Master::where('group','SELECCIONADOS')->first(); 


        $competitor = Competitor::find($idCompetitor);
        if($competitor === null){
            return response()->json(['status' => 'ER', 'message' => 'No existe el participante']);
        }

        $date = Carbon::now();
        
        $competition = Competition::find($idCompetition);
        if($competition === null){
            return response()->json(['status' => 'ER', 'message' => 'No existe el concurso']);
        }

        $cantidad = \DB::table('winner')->where('competition_id','=',$idCompetition)->count();

        //si esta activo y se va a cmbiar por ganador se valida limite
        if($competitor->status == "A"){
            if($cantidad >= $masterSelected->name){
                return response()->json(['status' => 'ER', 'message' => 'Se alcanzó el limite permitido de ganadores']);
            }
        }

        $winner = $competitor->winnerByIdCompetitionAndCompetitor($idCompetition, $competitor->id)->get();

        if($winner->count() == 1){
            //borramos el registro
            //dd($winner);
            if($winner[0]->delete()){

                $competitor = Competitor::find($competitor->id);
                $competitor->status = 'A';
                $competitor->push();

                return response()->json(['status' => 'OK', 'message' => 'Actualizado correctamente como participante(desde ganador)','text_status'=>'No seleccionado']);
            }else{
                return response()->json(['status' => 'ER', 'message' => 'No se pudo actualizar el ganador a partacipante']);
            }

        }else{
            if($cantidad >= $masterSelected->name){
                if($competition === null){
                    return response()->json(['status' => 'ER', 'message' => 'No se puede    actualizar ya que hay '.$cantidad.' participantes aprobados']);
                }
            }
            //creamos el registro
            $winner = new Winner();
            $winner->competition_id = $idCompetition;
            $winner->competitor_id = $idCompetitor;
            if($winner->save()){

                $competitor = Competitor::find($winner->competitor_id);
                $competitor->status = 'G';
                $competitor->push();

                return response()->json(['status' => 'OK', 'message' => 'Actualizado correctamente como ganador','text_status'=>'Ganador']);
            }else{
                return response()->json(['status' => 'ER', 'message' => 'No se pudo actualizar el ganador']);
            }
        }
        return response()->json(['status' => 'ER', 'message' => 'No se pudo actualizar el registro']);

    }

    public static function get_data_paginate_data($idCompetition){

        $masterPreselected = Master::where('group','PRESELECCIONADOS')->first();
        $masterSelected = Master::where('group','SELECCIONADOS')->first(); 
        
         $competitors = \DB::table('competitor')->whereIn('competitor.status',array('A','G'))->join('vote', 'competitor.id', '=', 'vote.competitor_id')->join('competition', 'vote.competition_id', '=', 'competition.id')->where('vote.competition_id','=',$idCompetition)->orderby('vote.vote','desc')->paginate($masterPreselected->name);

         $competitor_temp = '';
//dd($competitors);
         foreach ($competitors as $competitor) {
            
            $competitor->master_city = Master::find($competitor->city);
            $competitor->master_department = Master::find($competitor->department);
            //dd($competitor);
            $competitor -> master_city -> name = ucfirst(strtolower($competitor -> master_city -> name));
            $competitor -> master_department -> name = ucfirst(strtolower($competitor -> master_department -> name));

            $competitor_temp = new Competitor();
            error_log($idCompetition);

            $cantidad = \DB::table('winner')->where('competition_id','=',$idCompetition)->where('competitor_id','=',$competitor->competitor_id)->count();


            if($cantidad == 1){
                $competitor -> text_status = 'Ganador';
            }else{
                $competitor -> text_status = 'No Seleccionado';
            }

            $competitor -> competition_id = $idCompetition;

        }
        //dd($competitors);
         return $competitors;
    }
}
