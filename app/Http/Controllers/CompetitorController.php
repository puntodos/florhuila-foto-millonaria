<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Competition;
use App\Competitor;
use App\Master;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use SoapBox\Formatter\Formatter;


class CompetitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = Carbon::now();
        $masterRegxPage = Master::where('group','REGISTROSXPAGINA')->first(); 

         $competitions = Competition::orderBy('id', 'asc')->where('date_end','<',$date)->get();

        $filter = $request["filter"];

        if($filter == ""){
            $filter = "P";
        }
        
        $filter_selected_P = "";
        $filter_selected_A = "";
        $filter_selected_I = "";
        if($filter == "P"){
            $filter_selected_P = "selected";
        }
        if($filter == "A"){
            $filter_selected_A = "selected";
        }
        if($filter == "I"){
            $filter_selected_I = "selected";
        }

        $competitors = CompetitorController::get_data_paginate_data($filter);

        $competitions_all = Competition::all();
        
        return view("admin.competitor.index",['competitions'=>$competitions, 'competitors'=>$competitors, 'filter' => $filter,'filter_selected_P'=>$filter_selected_P,'filter_selected_A'=>$filter_selected_A,'filter_selected_I'=>$filter_selected_I,'competitions_all'=>$competitions_all]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function change_status(Request $request){
        $id = $request["id"];
        $status = $request["status"];

        $competitor = Competitor::find($id);
        if($competitor === null){
            return response()->json(['status' => 'ER', 'message' => 'No existe el participante']);
        }
        
        $competitor->status = $status;
        if($competitor->push()){
            if($status == "A"){
                CompetitorController::sendMailActivation($competitor);
            }
            if($status == "I"){
                //CompetitorController::sendMailRejected($competitor);
            }
            return response()->json(['status' => 'OK', 'message' => 'Actualizado correctamente','competitor_status'=>$competitor->status]);
        }else{
            return response()->json(['status' => 'ER', 'message' => 'No se pudo actualizar']);
        }
    }

    public static function get_data_paginate_data($filter = 'P'){
        error_log($filter);
        $masterRegxPage = Master::where('group','REGISTROSXPAGINA')->first(); 

         $competitors = Competitor::orderBy('id', 'asc')->where('status','=',$filter)->paginate($masterRegxPage->name);
         foreach ($competitors as $competitor) {
            /*$competitor->votes = '0';
            if($filter=='most'){
                $competitor->votes = $competitor->vote;
            }else{
                $vote = $competitor->votesByIdCompetitionAndCompetitor($competition->id, $competitor->id)->get();
                
                if($vote->count() == 1){
                    $competitor->votes = $vote[0]->vote;
                }else{
                    $competitor->votes = '0';
                }
            }
            */
            $competitor->master_city = Master::find($competitor->city);
            $competitor->master_department = Master::find($competitor->department);
            //dd($competitor);
            $competitor -> master_city -> name = ucfirst(strtolower($competitor -> master_city -> name));
            $competitor -> master_department -> name = ucfirst(strtolower($competitor -> master_department -> name));

            if($competitor -> status == 'P'){
                $competitor -> status_text = 'Pendiente';
            }
            if($competitor -> status == 'A'){
                $competitor -> status_text = 'Activo';
            }
            if($competitor -> status == 'I'){
                $competitor -> status_text = 'Inactivo';
            }   

        }
         return $competitors;
    }

    public static function sendMailActivation($competitor){
        $mailto = $competitor->email;
        $message = [];
        $message["name"] = $competitor->first_name;
        $message["permalink"] = $competitor->permalink;
        
        Mail::send('admin.competitor.activationmail', $message, function($message) use ($mailto)
        {
            $message->from('lafotomillonaria@florhuila.com', 'LaFotoMillonaria Florhuila');
            $message->to($mailto);
            $message->subject('¡Ya estás participando en la Foto Millonaria Florhuila!');
            //dd($mailto);
        });

    }

    public static function sendMailRejected($competitor){
        $mailto = $competitor->email;
        $message = [];
        $message["name"] = $competitor->first_name;
        $message["permalink"] = $competitor->permalink;
        
        Mail::send('admin.competitor.rejectmail', $message, function($message) use ($mailto)
        {
            $message->from('lafotomillonaria@florhuila.com', 'LaFotoMillonaria Florhuila');
            $message->to($mailto);
            $message->subject('¡Ya estás participando en la Foto Millonaria Florhuila!');
            //dd($mailto);
        });

    }

    public function export($competition){
        //$content = \View::make('order.txt.index')->with('order', $order);
        $competitors = \DB::table('competitor')->join('vote', 'competitor.id', '=', 'vote.competitor_id')->join('competition', 'vote.competition_id', '=', 'competition.id')->where('vote.competition_id','=',$competition)->orderby('vote.competitor_id','asc')->select('competitor.first_name as nombre','competitor.last_name as apellido','competitor.dni as cedula','competitor.phone as telefono','competitor.birth_date as fecha_nacimiento','competitor.department as departamento','competitor.city as ciudad','competitor.email as email','competitor.source as fuente','competitor.photo_id as foto','competitor.description as descripcion','competitor.address as direccion','competitor.status as estado','vote.vote as cantidad_votos')->get();
//DB::table('roles')->lists('title', 'name');
        $_masters =  \DB::table('master')->select('id','name')->get();
        $masters = Array();
        foreach ($_masters as $master) {
            $masters[$master->id] = $master->name;
        }
        //dd($masters);
        
        
        $file = fopen("archivo.csv", "w");
        fwrite($file, "nombre|apellido|cedula|telefono|fecha_nacimiento|departamento|ciudad|email|fuente|foto|descripcion|direccion|estado|cantidad_votos" . PHP_EOL);
        
        foreach ($competitors as $competitor) {

            $competitor -> descripcion = ucfirst(strtolower($competitor -> descripcion));
            $competitor -> descripcion = preg_replace( "/\r|\n/", "",($competitor -> descripcion));

            /*$competitor -> master_city -> name = ucfirst(strtolower($competitor -> master_city -> name));
            $competitor -> master_department -> name = ucfirst(strtolower($competitor -> master_department -> name));*/

            $linea = $competitor->nombre.'|';
            $linea .= $competitor->apellido.'|';
            $linea .= $competitor->cedula.'|';
            $linea .= $competitor->telefono.'|';
            $linea .= $competitor->fecha_nacimiento.'|';
            $linea .= $masters[$competitor->departamento].'|';
            $linea .= $masters[$competitor->ciudad].'|';
            $linea .= $competitor->email.'|';
            $linea .= $competitor->fuente.'|';
            $linea .= 'http://104.236.197.209/img/'.$competitor->foto.'|';
            $linea .= $competitor->descripcion.'|';
            $linea .= $competitor->direccion.'|';
            $linea .= $competitor->estado.'|';
            $linea .= $competitor->cantidad_votos;
            fwrite($file, $linea . PHP_EOL);
        }

        fclose($file);
        
        return \Response::download('archivo.csv');
        
    }
}
