<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Competition;
use App\Http\Requests\CompetitionRequest;
use App\Http\Requests\CompetitionUpdateRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Flash;
use App\Http\Requests;
use App\Master;

class CompetitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $date = Carbon::now();
        $masterRegxPage = Master::where('group','REGISTROSXPAGINA')->first(); 

         $competitions = Competition::orderBy('id', 'asc')->where('date_end','<',$date)->get();
        
         $competitions_all = Competition::all();

        return view("admin.competition.index",['competitions'=>$competitions,'competitions_all'=>$competitions_all]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $competitions = Competition::all();

        
        return view("admin.competition.create",['competitions'=>$competitions,'competitions_all'=>$competitions]);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompetitionRequest $request)
    {
        $competition = new Competition($request->all());
        $competition->date_init = Carbon::parse($competition->date_init);
        $competition->date_end = Carbon::parse($competition->date_end);

        if($competition->save()){
            Flash::success('Concurso creado con éxito.');
            return redirect()->route('admin.competition.index');
        }else{
            return Redirect::back()->withErrors(['se presentaron errores en la creación del concurso']);
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $competitions = Competition::all();
        $competition = Competition::find($id);
        if($competition != null){
            return view("admin.competition.edit",['competitions'=>$competitions,'competition'=>$competition,'competitions_all'=>$competitions]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompetitionRequest $request)
    {
        $competition = Competition::find($request->id);
        $competition->date_init = Carbon::parse($request->date_init);
        $competition->date_end = Carbon::parse($request->date_end);
        
        if($competition->push()){
            Flash::success('Concurso actualizado con éxito.');
            return redirect()->route('admin.competition.index');
        }else{
            return Redirect::back()->withErrors(['no pudo ser actualizado el concurso']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
