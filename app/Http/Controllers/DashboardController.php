<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Competition;
use Carbon\Carbon;
use App\Master;


class DashboardController extends Controller
{
    //
    public function index(){
    	$date = Carbon::now();
        $masterRegxPage = Master::where('group','REGISTROSXPAGINA')->first(); 

         $competitions = Competition::orderBy('id', 'asc')->where('date_end','<',$date)->get();
         $competitions_all = Competition::all();
    	return view("admin.dashboard",['competitions'=>$competitions,'competitions_all'=>$competitions_all]);
    }
}
