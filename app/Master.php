<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master extends Model
{
    protected $table = "master";
    protected $fillable = ['id','name','group','id_father'];

    public static function get_master_by_group_and_id( $group ,$id_father){
    	$where = ['group'=>$group,'id_father'=>$id_father];
    	return Master::where($where)->get();
    }
}